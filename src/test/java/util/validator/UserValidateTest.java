package util.validator;

import entity.enums.Role;
import org.junit.Test;

public class UserValidateTest {

    @Test
    public void validatePassport() {

    }

    @Test
    public void validatePassword() {
    }

    @Test
    public void validateDrivingExperience() {
    }

    @Test
    public void validateName() {
    }

    @Test
    public void validateAge() {
    }

    @Test (expected = ValidatorException.class)
    public void validateEmail() throws ValidatorException {
        String source = "hello.world_gmail.com";

        UserValidator userValidator = new UserValidator();
        String expected = userValidator.validateEmail(source, true);
        userValidator.valid("Uncorrcect form");
    }

    @Test
    public void validatePhone() {
        String source = "+385 29 346 4364";
        UserValidator userValidator = new UserValidator();
        System.out.println(userValidator.validatePhone(source, true));
        System.out.println(userValidator.isPhone(source));
    }

    @Test
    public void validateStatus() {
        System.out.println(Role.valueOf("MANAGER"));
        System.out.println(new GeneralValidator().isInEnum( "MANAGER", Role.class));
    }

    @Test
    public void validateRole() {
    }
}