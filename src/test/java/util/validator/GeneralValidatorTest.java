package util.validator;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class GeneralValidatorTest {

    @Test
    public void isNumeric() {
    }

    @Test
    public void rowExist() {
    }

    @Test
    public void isLong() {
    }

    @Test
    public void isFloat() {
    }

    @Test
    public void isInt() {
    }

    @Test
    public void isLocalDate() {
    }

    @Test
    public void isPhone() {
        String source = "+375 29 405 4969";
        String source2 = "+378 29 458 2734";

        GeneralValidator generalValidator = new GeneralValidator();

        boolean actual = generalValidator.isPhone(source);
        boolean actual2 = generalValidator.isPhone(source2);

        Assert.assertTrue(actual);
        Assert.assertFalse(actual2);
    }

    @Test
    public void isPassport() {
        String source = "MP5462348";
        String source2 = "GK876345";

        GeneralValidator generalValidator = new GeneralValidator();

        boolean actual = generalValidator.isPassport(source);
        boolean actual2 = generalValidator.isPassport(source2);

        Assert.assertTrue(actual);
        Assert.assertFalse(actual2);
    }

    @Test
    public void isCorrectPeriod() {
    }

    @Test
    public void isEmail() {
        String source = "p.finskiy.vironit@vironit.ru  ";
        String source2 = "helloworld.ru";

        GeneralValidator generalValidator = new GeneralValidator();

        boolean actual = generalValidator.isEmail(source);
        boolean actual2 = generalValidator.isEmail(source2);

        Assert.assertTrue(actual);
        Assert.assertFalse(actual2);
    }

    @Test
    public void isInterval() {
    }

    @Test
    public void testIsInterval() {
    }

    @Test
    public void exist() {
        System.out.println(";"+"MANAGER           ".trim()+";");
    }

    @Test
    public void isInEnum() {
    }

    @Test
    public void validateId() {
    }

    @Test
    public void validNotNull() {
    }

    @Test
    public void valid() {
    }
}