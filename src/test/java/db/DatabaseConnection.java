package db;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DatabaseConnection {
    private static DatabaseConnection db;
    private Connection connection;
    private Statement statement;

    private DatabaseConnection() {
        init();
    }

    private void init() {
        Properties props = null;
        try {
            props = getProps();
        } catch (IOException e) {
            System.out.println("Load config Failed");
            System.out.println(e);
        }
        String dbDriver = props.getProperty("driver");

        //get Driver class
        try {
            Class.forName(dbDriver);
            System.out.println("Driver loaded successfully!");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Driver load failed...");
        }

        //connect to db
        try {
            connection = getConnection(props);
            statement = connection.createStatement();
        } catch (SQLException e) {
            System.out.println("Connection Failed");
            System.out.println(e);
        }

        if (connection != null) {
            System.out.println("You successfully connected to database now");
        } else {
            System.out.println("Failed to make connection to database");
        }
    }

    private Connection getConnection(Properties props) throws SQLException {
        String url = props.getProperty("url");
        String username = props.getProperty("username");
        String password = props.getProperty("password");

        //choose on of method, depends on the number of parameters
        if (url != null && username != null && password != null) {
            return DriverManager.getConnection(url, username, password);
        } else if (url != null) {
            return DriverManager.getConnection(url);
        } else {
            throw new SQLException();
        }
    }

    private Properties getProps() throws IOException {
        Properties props = new Properties();
        try(InputStream in = getClass().getClassLoader().getResourceAsStream("database.properties")) {
            props.load(in);
        }
        return props;
    }

    public static synchronized DatabaseConnection getDb() {
        if (db == null) {
            synchronized (DatabaseConnection.class) {
                if (db == null)
                    db = new DatabaseConnection();
            }
        }
        return db;
    }

    public Connection getConnection() {
        return connection;
    }

    public Statement getStatement() {
        return statement;
    }
}
