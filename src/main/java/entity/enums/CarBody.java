package entity.enums;

public enum CarBody {
    MICRO_COMPACT_CAR,
    LANDAU,
    VAN,
    MINIVAN,
    CABRIOLET,
    LIMOUSINE,
    PICKUP,
    CROSSOVER,
    SEDAN,
    SUV
}