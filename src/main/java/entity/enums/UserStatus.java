package entity.enums;

public enum UserStatus {
    BUSY,
    FREE,
    FIRED,
    VACATION
}
