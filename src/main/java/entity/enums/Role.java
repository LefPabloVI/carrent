package entity.enums;

public enum Role {
    SUPERADMIN,
    MANAGER,
    EMPLOYEE
}
