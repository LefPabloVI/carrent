package entity.enums;

public enum OrderStatus {
    RESERVATION,
    CLOSED,
    PERFORMED,
    FRAMED,
    COMPLETES
}
