package entity.enums;

public enum Period {
    F("1-3"), S("4-7"), T("8-15"), FO("16-30");
    private String text;
    Period(String text){
        this.text = text;
    }
    public String getText(){ return text;}
}
