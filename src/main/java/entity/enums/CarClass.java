package entity.enums;

public enum CarClass {
    LUX,
    ECO,
    MID,
    EXC
}
