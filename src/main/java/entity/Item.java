package entity;

public class Item {
    entity.hibernate.Car car;
    Float max;
    Float min;
    String urn;

    public Item(entity.hibernate.Car car, Float max, Float min, String urn) {
        this.car = car;
        this.max = max;
        this.min = min;
        this.urn = urn;
    }

    public entity.hibernate.Car getCar() {
        return car;
    }

    public void setCar(entity.hibernate.Car car) {
        this.car = car;
    }

    public Float getMax() {
        return max;
    }

    public void setMax(Float max) {
        this.max = max;
    }

    public Float getMin() {
        return min;
    }

    public void setMin(Float min) {
        this.min = min;
    }

    public String getUrn() {
        return urn;
    }

    public void setUrn(String urn) {
        this.urn = urn;
    }
}
