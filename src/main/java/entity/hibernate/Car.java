package entity.hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "car")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "car_class")
    @Size(min = 3, max = 3)
    @NotNull
    private String carClass;
    @NotNull
    private String model;
    private String body;
    private String transmission;
    private String brand;
    private String color;
    private String year;
    private String passengers;
    private String doors;

    @Column(name = "fuel_consumption")
    private String fuelConsumption;

    @Column(name = "type_engine_displacement")
    private String typeAndEngineDisplacement;
    private String options;


    public Car() {

    }

    public Car(String carClass, String model, String body, String transmission, String brand, String color, String year, String passengers, String doors, String fuelConsumption, String typeAndEngineDisplacement, String options) {
        this.model = model;
        this.carClass = carClass;
        this.body = body;
        this.transmission = transmission;
        this.brand = brand;
        this.color = color;
        this.year = year;
        this.passengers = passengers;
        this.doors = doors;
        this.fuelConsumption = fuelConsumption;
        this.typeAndEngineDisplacement = typeAndEngineDisplacement;
        this.options = options;
    }

    public Integer getId() {
        return id;
    }

    public String getCarClass() {
        return carClass;
    }

    public void setCarClass(String carClass) {
        this.carClass = carClass;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPassengers() {
        return passengers;
    }

    public void setPassengers(String passengers) {
        this.passengers = passengers;
    }

    public String getDoors() {
        return doors;
    }

    public void setDoors(String doors) {
        this.doors = doors;
    }

    public String getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(String fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public String getTypeAndEngineDisplacement() {
        return typeAndEngineDisplacement;
    }

    public void setTypeAndEngineDisplacement(String typeAndEngineDisplacement) {
        this.typeAndEngineDisplacement = typeAndEngineDisplacement;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "hibernate.Car {" +
                "id=" + id +
                "carClass '" + carClass + '\'' +
                "model '" + model + '\'' +
                "body '" + body + '\'' +
                "transmission '" + transmission + '\'' +
                "brand '" + brand + '\'' +
                "color '" + color + '\'' +
                "year '" + year + '\'' +
                "passengers '" + passengers + '\'' +
                "doors '" + doors + '\'' +
                "fuelConsumption '" + fuelConsumption + '\'' +
                "typeAndEngineDisplacement '" + typeAndEngineDisplacement + '\'' +
                "options '" + options + '\'' +
                "}"
                ;
    }
}
