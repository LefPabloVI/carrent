package entity.hibernate;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private Integer age;
    private String phone;
    private String passport;

    @Column(name = "driver_experience")
    private Integer drivingExperience;

    @Column(name = "driver_license")
    private String driverLicense;
    private String password;
    private String email;

    @Column(name = "employee_code")
    private Integer employeeCode;
    private String status;
    private String role;

    public User() {
    }

    public User(String name, Integer age, String phone, String passport, Integer drivingExperience, String driverLicense, String password, String email, Integer employeeCode, String status, String role) {
        this.name = name;
        this.age = age;
        this.phone = phone;
        this.passport = passport;
        this.drivingExperience = drivingExperience;
        this.driverLicense = driverLicense;
        this.password = password;
        this.email = email;
        this.employeeCode = employeeCode;
        this.status = status;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public Integer getDrivingExperience() {
        return drivingExperience;
    }

    public void setDrivingExperience(Integer drivingExperience) {
        this.drivingExperience = drivingExperience;
    }

    public String getDriverLicense() {
        return driverLicense;
    }

    public void setDriverLicense(String driverLicense) {
        this.driverLicense = driverLicense;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(Integer employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "hibernate.User {" +
                "id=" + id +
                ", name '" + name + '\'' +
                ", age=" + age +
                ", phone '" + phone + '\'' +
                ", passport '" + passport + '\'' +
                ", drivingExperience=" + drivingExperience +
                ", driverLicense '" + driverLicense + '\'' +
                ", password '" + password + '\'' +
                ", email '" + email + '\'' +
                ", employeeCode=" + employeeCode +
                ", status '" + status + '\'' +
                '}';
    }
}
