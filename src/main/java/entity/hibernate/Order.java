package entity.hibernate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "rent")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "client_id")
    private Integer client;

    @Column(name = "car_id")
    private Integer car;

    @Column(name = "start_date")
    private LocalDate startPeriod;

    @Column(name = "end_date")
    private LocalDate endPeriod;
    private Integer kilometers;
    private Float price;
    private String status;
    private LocalDateTime updated;

    public Order() {
    }

    public Order(Integer client, Integer car, LocalDate startPeriod, LocalDate endPeriod, Integer kilometers, Float price, String status, LocalDateTime updated) {
        this.client = client;
        this.car = car;
        this.startPeriod = startPeriod;
        this.endPeriod = endPeriod;
        this.updated = updated;
        this.kilometers = kilometers;
        this.price = price;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public Integer getClient() {
        return client;
    }

    public void setClient(Integer client) {
        this.client = client;
    }

    public Integer getCar() {
        return car;
    }

    public void setCar(Integer car) {
        this.car = car;
    }

    public LocalDate getStartPeriod() {
        return startPeriod;
    }

    public void setStartPeriod(LocalDate startPeriod) {
        this.startPeriod = startPeriod;
    }

    public LocalDate getEndPeriod() {
        return endPeriod;
    }

    public void setEndPeriod(LocalDate endPeriod) {
        this.endPeriod = endPeriod;
    }

    public Integer getKilometers() {
        return kilometers;
    }

    public void setKilometers(Integer kilometers) {
        this.kilometers = kilometers;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }
}
