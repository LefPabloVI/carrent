package entity.hibernate;

import javax.persistence.*;

@Entity
@Table(name = "image")
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String entity;

    @Column(name = "element_id")
    private Integer elementId;
    private String type;
    private String urn;

    public Image() {
    }

    public Image(String entity, Integer elementId, String type, String urn) {
        this.entity = entity;
        this.elementId = elementId;
        this.type = type;
        this.urn = urn;
    }

    public Integer getId() {
        return id;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public Integer getElementId() {
        return elementId;
    }

    public void setElementId(Integer elementId) {
        this.elementId = elementId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrn() {
        return urn;
    }

    public void setUrn(String urn) {
        this.urn = urn;
    }
}
