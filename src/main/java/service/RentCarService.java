package service;

import db.dao.hibernate.CarDao;
import db.dao.hibernate.impl.CarDaoImpl;
import db.dao.hibernate.ImageDao;
import db.dao.hibernate.impl.ImageDaoImpl;
import db.dao.hibernate.PriceDao;
import db.dao.hibernate.impl.PriceDaoImpl;
import entity.hibernate.Car;
import entity.hibernate.Image;
import entity.hibernate.Price;
import util.validator.CarValidator;
import util.validator.GeneralValidator;
import util.validator.PriceValidator;
import util.validator.ValidatorException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class RentCarService {
    private final ImageDao imageDAO = new ImageDaoImpl();
    private final CarDao carDAO = new CarDaoImpl();
    private final PriceDao priceDAO = new PriceDaoImpl();

    private final GeneralValidator generalValidator = new GeneralValidator();
    private final CarValidator carValidator = new CarValidator();
    private final PriceValidator priceValidator = new PriceValidator();

    public Car getCar(HttpServletRequest request) throws ValidatorException {
        long id = carValidator.validateId(request.getParameter("car_id"), true);
        carValidator.valid("Rent car form is not correct: ");
        return carDAO.findById((int) id);
    }

    public List<Image> getImages(HttpServletRequest request) throws ValidatorException {
        long id = generalValidator.validateId(request.getParameter("car_id"), "Id must be not null", "Id must be 'long': " + request.getParameter("car_id"));
        generalValidator.valid("Rent car form is not correct: ");
        return imageDAO.findByCar((int) id);
    }

    public List<Price> getPrices(HttpServletRequest request) throws ValidatorException {
        long id = priceValidator.validateId(request.getParameter("car_id"), true);
        priceValidator.valid("Rent car form is not correct: ");
        return priceDAO.findByCar((int) id);
    }
}
