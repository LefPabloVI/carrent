package service.crud;

import db.dao.hibernate.UserDao;
import db.dao.hibernate.impl.UserDaoImpl;
import entity.hibernate.User;
import entity.enums.Role;
import util.validator.UserValidator;
import util.validator.ValidatorException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class UserService {
    private final UserValidator userValidator = new UserValidator();
    private final UserDao userDao = new UserDaoImpl();

    public void update(HttpServletRequest req) throws ValidatorException {
        long id = userValidator.validateId(req.getParameter("id"), true);
        User user = userDao.findById((int) id);

        String name = userValidator.validateName(req.getParameter("name"), true);
        Integer age = userValidator.validateAge(req.getParameter("age"), false);
        String phone = userValidator.validatePhone(req.getParameter("phone"), true);
        String passport = userValidator.validatePassport(req.getParameter("passport"), false);
        Integer drivingExperience = userValidator.validateDrivingExperience(req.getParameter("drivingExperience"), false);
        String driverLicense = userValidator.validateDriverLicense(req.getParameter("driverLicense"), false);
        String password = userValidator.validatePassword(req.getParameter("password"), false);
        String email = userValidator.validateEmail(req.getParameter("email"), true);
        Integer employeeCode = userValidator.validateEmployeeCode(req.getParameter("employeeCode"), false);
        String status = userValidator.validateStatus(req.getParameter("status"), false);
        String role = userValidator.validateRole(req.getParameter("role"), false);

        //this method throw exception if not all params valid
        userValidator.valid("User form is not correct: ");

        user.setName(name);
        user.setAge(age);
        user.setPhone(phone);
        user.setPassport(passport);
        user.setDrivingExperience(drivingExperience);
        user.setDriverLicense(driverLicense);
        user.setPassword(password);
        user.setEmail(email);
        user.setEmployeeCode(employeeCode);
        user.setStatus(status);
        user.setRole(role);

        userDao.update(user);
    }

    public void create(HttpServletRequest req) throws ValidatorException {
        String name = userValidator.validateName(req.getParameter("name"), true);
        Integer age = userValidator.validateAge(req.getParameter("age"), false);
        String phone = userValidator.validatePhone(req.getParameter("phone"), true);
        String passport = userValidator.validatePassport(req.getParameter("passport"), false);
        Integer drivingExperience = userValidator.validateDrivingExperience(req.getParameter("drivingExperience"), false);
        String driverLicense = userValidator.validateDriverLicense(req.getParameter("driverLicense"), false);
        String password = userValidator.validatePassword(req.getParameter("password"), false);
        String email = userValidator.validateEmail(req.getParameter("email"), true);
        Integer employeeCode = userValidator.validateEmployeeCode(req.getParameter("employeeCode"), false);
        String status = userValidator.validateStatus(req.getParameter("status"), false);
        String role = userValidator.validateRole(req.getParameter("role"), false);

        //this method throw exception if not all params valid
        userValidator.valid("User form is not correct: ");
        User user = new User(name, age, phone, passport, drivingExperience, driverLicense, password, email, employeeCode, status, role);

        userDao.save(user);
    }

    public void delete(HttpServletRequest req) throws ValidatorException {
        long id = userValidator.validateId(req.getParameter("id"), true);

        //this method throw exception if not all params valid
        userValidator.valid("User form is not correct: ");

        userDao.delete(userDao.findById((int) id));
    }

    public User get(HttpServletRequest req) throws ValidatorException {
        long id = userValidator.validateId(req.getParameter("id"), true);
        userValidator.valid("User form is not correct: ");
        return userDao.findById((int) id);
    }

    public List<User> getAll() {
        return userDao.findAll();
    }

    public boolean accessToRow(User currentUser ,User user) {
        if (user == null) {
            return true;
        }
        String userRole = currentUser.getRole();
        String role = user.getRole();
        switch (userRole) {
            case "SUPERADMIN":

                break;
            case "MANAGER":
                if (role.equalsIgnoreCase(Role.SUPERADMIN.name())) {
                    return false;
                }
                break;
            case "EMPLOYEE":
                if (role.equalsIgnoreCase(Role.SUPERADMIN.name()) || role.equalsIgnoreCase(Role.MANAGER.name())) {
                    return false;
                }
                break;
        }
        return true;
    }
}
