package service.crud;

import db.dao.hibernate.OrderDao;
import db.dao.hibernate.impl.OrderDaoImpl;
import entity.hibernate.Order;
import util.validator.OrderValidator;
import util.validator.PriceValidator;
import util.validator.ValidatorException;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class OrderService {
    private final OrderDao orderDAO = new OrderDaoImpl();
    private final OrderValidator orderValidator = new OrderValidator();
    private final PriceValidator priceValidator = new PriceValidator();

    public void update(HttpServletRequest request) throws ValidatorException {
        long orderId = orderValidator.validateId(request.getParameter("id"), true);
        long clientId = orderValidator.validateId(request.getParameter("user"), "User id must be not null", "User id must be a 'long': " + request.getParameter("car"));
        long carId = orderValidator.validateId(request.getParameter("car"), "Car id must be not null", "Car id must be a 'long': " + request.getParameter("car"));
        LocalDate startDate = orderValidator.validateStartDate(request.getParameter("start"), false);
        LocalDate endDate = orderValidator.validateEndDate(request.getParameter("end"), false);
        orderValidator.validatePeriod(startDate, endDate);
        Integer kilometers = orderValidator.validateKilometers(request.getParameter("kilometers"), false);
        Float price = priceValidator.validatePrice(request.getParameter("price"), true);
        String status = orderValidator.validateStatus(request.getParameter("status"), false);;
        LocalDateTime updated = LocalDateTime.now();

        //this method throw exception if not all params valid
        orderValidator.mergeDetails(priceValidator);
        priceValidator.clean();
        orderValidator.valid("Order form is not correct: ");

        Order order = orderDAO.findById((int) orderId);

        order.setClient((int) clientId);
        order.setCar((int) carId);
        order.setStartPeriod(startDate);
        order.setEndPeriod(endDate);
        order.setKilometers(kilometers);
        order.setPrice(price);
        order.setStatus(status);
        order.setUpdated(updated);

        orderDAO.update(order);
    }

    public void create(HttpServletRequest request) throws ValidatorException {
        long clientId = orderValidator.validateId(request.getParameter("user"), "User id must be not null", "User id must be a 'long': " + request.getParameter("car"));
        long carId = orderValidator.validateId(request.getParameter("car"), "Car id must be not null", "Car id must be a 'long': " + request.getParameter("car"));
        LocalDate startDate = orderValidator.validateStartDate(request.getParameter("start"), false);
        LocalDate endDate = orderValidator.validateEndDate(request.getParameter("end"), false);
        orderValidator.validatePeriod(startDate, endDate);
        Integer kilometers = orderValidator.validateKilometers(request.getParameter("kilometers"), false);
        Float price = priceValidator.validatePrice(request.getParameter("price"), true);
        String status = orderValidator.validateStatus(request.getParameter("status"), false);
        LocalDateTime updated = LocalDateTime.now();

        //this method throw exception if not all params valid
        orderValidator.mergeDetails(priceValidator);
        priceValidator.clean();
        orderValidator.valid("Order form is not correct: ");

        Order order = new Order((int) clientId,(int) carId,  startDate, endDate, kilometers,  price, status, updated);

        orderDAO.save(order);
    }

    public void delete(HttpServletRequest request) throws ValidatorException {
        long id = orderValidator.validateId(request.getParameter("id"), true);

        //this method throw exception if not all params valid
        orderValidator.valid("Order form is not correct: ");

        orderDAO.delete(orderDAO.findById((int) id));
    }

    public Order get(HttpServletRequest request) throws ValidatorException {
        long id = orderValidator.validateId(request.getParameter("id"), true);
        orderValidator.valid("Order form is not correct: ");
        return orderDAO.findById((int) id);
    }

    public List<Order> getAll() {
        return orderDAO.findAll();
    }
}
