package service.crud;

import db.dao.hibernate.PriceDao;
import db.dao.hibernate.impl.PriceDaoImpl;
import entity.hibernate.Price;
import util.validator.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class PriceService {
    private final PriceDao priceDAO = new PriceDaoImpl();
    private final PriceValidator priceValidator = new PriceValidator();

    public void update(HttpServletRequest request) throws ValidatorException {
        long priceId = priceValidator.validateId(request.getParameter("id"), true);
        long carId = priceValidator.validateId(request.getParameter("car"), "Car id must be not null", "Car id must be a long: " + request.getParameter("car"));;
        String period = priceValidator.validatePeriod(request.getParameter("period"), true);
        Float cost = priceValidator.validatePrice(request.getParameter("price"), false);

        //this method throw exception if not all params valid
        priceValidator.valid("Price from is not correct: ");
        Price price = priceDAO.findById((int) priceId);

        price.setIdCar((int) carId);
        price.setPeriod(period);
        price.setPrice(cost);

        priceDAO.update(price);
    }

    public void create(HttpServletRequest request) throws ValidatorException {
        long carId = priceValidator.validateId(request.getParameter("car"), "Car id must be not null", "Car id must be a long: " + request.getParameter("car"));;
        String period = priceValidator.validatePeriod(request.getParameter("period"), true);
        Float cost = priceValidator.validatePrice(request.getParameter("price"), false);

        //this method throw exception if not all params valid
        priceValidator.valid("Price from is not correct: ");

        Price price = new Price((int) carId, period, cost);

        priceDAO.save(price);
    }

    public void delete(HttpServletRequest request) throws ValidatorException {
        long id = priceValidator.validateId(request.getParameter("id"), true);

        //this method throw exception if not all params valid
        priceValidator.valid("Price from is not correct: ");

        priceDAO.delete(priceDAO.findById((int) id));
    }

    public Price get(HttpServletRequest request) throws ValidatorException {
        long id = priceValidator.validateId(request.getParameter("id"), true);
        priceValidator.valid("Price from is not correct: ");
        return priceDAO.findById((int) id);
    }

    public List<Price> getAll() {
        return priceDAO.findAll();
    }

    public List<Price> getByCar(int id) {
        return priceDAO.findByCar(id);
    }
}
