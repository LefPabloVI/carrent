package service.crud;

import db.dao.hibernate.ReservationDao;
import db.dao.hibernate.impl.ReservationDaoImpl;
import entity.hibernate.Reservation;
import util.validator.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;

public class ReservationService {
    private final ReservationDao reservationDAO = new ReservationDaoImpl();
    private final OrderValidator orderValidator = new OrderValidator();
    private final UserValidator userValidator = new UserValidator();

    public void update(HttpServletRequest request) throws ValidatorException {
        long id = userValidator.validateId(request.getParameter("id"), "Reservation id must be not null", "Reservation id must be a 'long': " + request.getParameter("id"));
        long car = userValidator.validateId(request.getParameter("car_id"), "Car id must be not null", "Car id must be a 'long:'" + request.getParameter("car_id"));
        String name = userValidator.validateName(request.getParameter("name"), true);
        String phone = userValidator.validatePhone(request.getParameter("phone"), true);
        String email = userValidator.validateEmail(request.getParameter("email"), true);
        LocalDate startDate = orderValidator.validateStartDate(request.getParameter("start"), true);
        LocalDate endDate = orderValidator.validateEndDate(request.getParameter("end"), true);
        orderValidator.validatePeriod(startDate, endDate);

        //this method throw exception if not all params valid
        orderValidator.mergeDetails(userValidator);
        userValidator.clean();
        orderValidator.valid("Reservation form is not correct: ");

        Reservation reservation = reservationDAO.findById((int) id);

        reservation.setCar((int) car);
        reservation.setName(name);
        reservation.setPhone(phone);
        reservation.setEmail(email);
        reservation.setStartDate(startDate);
        reservation.setEndDate(endDate);

        reservationDAO.update(reservation);
    }

    public void create(HttpServletRequest request) throws ValidatorException {
        long car = userValidator.validateId(request.getParameter("car_id"), "Car id must be not null", "Car id must be a 'long:'" + request.getParameter("car_id"));
        String name = userValidator.validateName(request.getParameter("name"), true);
        String phone = userValidator.validatePhone(request.getParameter("phone"), true);
        String email = userValidator.validateEmail(request.getParameter("email"), true);
        LocalDate startDate = orderValidator.validateStartDate(request.getParameter("start"), true);
        LocalDate endDate = orderValidator.validateEndDate(request.getParameter("end"), true);
        orderValidator.validatePeriod(startDate, endDate);

        //this method throw exception if not all params valid
        orderValidator.mergeDetails(userValidator);
        userValidator.clean();
        orderValidator.valid("Reservation form is not correct: ");
        Reservation reservation = new Reservation((int) car ,name, phone, email, startDate, endDate);

        reservationDAO.save(reservation);
    }

    public void delete(HttpServletRequest request) throws ValidatorException {
        orderValidator.validAccess(request, "MANAGER", "SUPERADMIN");
        orderValidator.valid("Access error: ");

        long id = userValidator.validateId(request.getParameter("id"), "Reservation id must be not null", "Reservation id must be a 'long': " + request.getParameter("id"));

        //this method throw exception if not all params valid
        orderValidator.valid("Reservation form is not correct: ");

        reservationDAO.delete(reservationDAO.findById((int) id));
    }

    public Reservation get(HttpServletRequest request) throws ValidatorException {
        long id = userValidator.validateId(request.getParameter("id"), "Reservation id must be not null", "Reservation id must be a 'long': " + request.getParameter("id"));
        orderValidator.valid("Reservation form is not correct: ");
        return reservationDAO.findById((int) id);
    }

    public List<Reservation> getAll() {
        return reservationDAO.findAll();
    }
}
