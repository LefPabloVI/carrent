package service.crud;

import db.dao.hibernate.ImageDao;
import db.dao.hibernate.impl.ImageDaoImpl;
import entity.hibernate.Image;
import util.validator.PriceValidator;
import util.validator.ValidatorException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ImageService {
    private final PriceValidator priceValidator = new PriceValidator();
    private final ImageDao imageDAO = new ImageDaoImpl();

    public void update(HttpServletRequest req) throws ValidatorException {
        long imageId = Long.parseLong(req.getParameter("id"));
//        String entity = req.getParameter("entity");
        long elementId = Long.parseLong(req.getParameter("elementId"));
        String type = req.getParameter("type");
        String urn = req.getParameter("urn");

        //this method throw exception if not all params valid
        priceValidator.valid("Fine form is not correct: ");
        Image image = imageDAO.findById((int) imageId);

        image.setElementId((int) elementId);
        image.setType(type);
        image.setUrn(urn);

        imageDAO.update(image);
    }

    public void create(HttpServletRequest req) throws ValidatorException {
//        String entity = req.getParameter("entity");
        long elementId = Long.parseLong(req.getParameter("elementId"));
        String type = req.getParameter("type");
        String urn = req.getParameter("urn");

        //this method throw exception if not all params valid
        priceValidator.valid("Fine form is not correct: ");
        Image image = new Image("" ,(int) elementId, type, urn);

        imageDAO.save(image);
    }

    public void delete(HttpServletRequest req) throws ValidatorException {
        long id = priceValidator.validateId(req.getParameter("id"), "Image id must be not null", "Image id must be a 'long': " + req.getParameter("id"));

        //this method throw exception if not all params valid
        priceValidator.valid("Fine form is not correct: ");

        imageDAO.delete(imageDAO.findById((int) id));
    }

    public Image get(HttpServletRequest req) throws ValidatorException {
        long id = priceValidator.validateId(req.getParameter("id"), "Image id must be not null", "Image id must be a 'long': " + req.getParameter("id"));
        priceValidator.valid("Fine form is not correct: ");
        return imageDAO.findById((int) id);
    }

    public List<Image> getAll() {
        return imageDAO.findAll();
    }

    public List<Image> getByCar(int id) {
        return imageDAO.findByCar(id);
    }
}
