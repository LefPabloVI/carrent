package service.crud;

import db.dao.hibernate.CarDao;
import db.dao.hibernate.impl.CarDaoImpl;
import entity.hibernate.Car;
import util.validator.CarValidator;
import util.validator.ValidatorException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class CarService {
    private final CarDao carDao = new CarDaoImpl();
    private final CarValidator carValidator = new CarValidator();
    
    public void update(HttpServletRequest req) throws ValidatorException {
        long id = carValidator.validateId(req.getParameter("id"), true);
        Car car = carDao.findById((int) id);

        String carClass = carValidator.validateCarClass(req.getParameter("carClass"), true);
        String body = carValidator.validateBody(req.getParameter("body"), false);
        String transmission = carValidator.validateTransmission(req.getParameter("transmission"), true);
        String brand = carValidator.validateBrand(req.getParameter("brand"), true);
        String color = carValidator.validateColor(req.getParameter("color"), false);
        String year = carValidator.validateYear(req.getParameter("year"), true);
        String passengers = carValidator.validatePassengers(req.getParameter("passengers"), true);
        String doors = carValidator.validateDoors(req.getParameter("doors"), true);
        String fuelConsumption = carValidator.validateFuelConsumption(req.getParameter("fuelConsumption"), true);
        String typeAndEngineDisplacement = carValidator.validateTypeAndEngineDisplacement(req.getParameter("typeAndEngineDisplacement"), true);
        String options = carValidator.validateOptions(req.getParameter("options"), false);
        String model = carValidator.validateModel(req.getParameter("model"), false);

        //this method throw exception if not all params valid
        carValidator.valid("Car form is not correct: ");

        car.setCarClass(carClass);
        car.setBody(body);
        car.setTransmission(transmission);
        car.setBrand(brand);
        car.setColor(color);
        car.setYear(year);
        car.setPassengers(passengers);
        car.setDoors(doors);
        car.setFuelConsumption(fuelConsumption);
        car.setTypeAndEngineDisplacement(typeAndEngineDisplacement);
        car.setOptions(options);
        car.setModel(model);

        carDao.update(car);
    }

    public void create(HttpServletRequest req) throws ValidatorException {
        String carClass = carValidator.validateCarClass(req.getParameter("carClass"), true);
        String body = carValidator.validateBody(req.getParameter("body"), false);
        String transmission = carValidator.validateTransmission(req.getParameter("transmission"), true);
        String brand = carValidator.validateBrand(req.getParameter("brand"), true);
        String color = carValidator.validateColor(req.getParameter("color"), false);
        String year = carValidator.validateYear(req.getParameter("year"), true);
        String passengers = carValidator.validatePassengers(req.getParameter("passengers"), true);
        String doors = carValidator.validateDoors(req.getParameter("doors"), true);
        String fuelConsumption = carValidator.validateFuelConsumption(req.getParameter("fuelConsumption"), true);
        String typeAndEngineDisplacement = carValidator.validateTypeAndEngineDisplacement(req.getParameter("typeAndEngineDisplacement"), true);
        String options = carValidator.validateOptions(req.getParameter("options"), false);
        String model = carValidator.validateModel(req.getParameter("model"), false);

        //this method throw exception if not all params valid
        carValidator.valid("Car form is not correct: ");
        Car car = new Car(carClass, model, body, transmission, brand, color, year, passengers, doors, fuelConsumption, typeAndEngineDisplacement, options);

        carDao.save(car);
    }

    public void delete(HttpServletRequest req) throws ValidatorException {
        long id = carValidator.validateId(req.getParameter("id"), true);

        //this method throw exception if not all params valid
        carValidator.valid("Car form is not correct: ");

        carDao.delete(carDao.findById((int) id));
    }

    public Car get(HttpServletRequest req)  throws ValidatorException {
        long id = carValidator.validateId(req.getParameter("id"), true);
        carValidator.valid("Car form is not correct: ");
        return carDao.findById((int) id);
    }

    public List<Car> getAll() {
        return carDao.findAll();
    }

    public List<Car> getPage(int page, int count) {
        return carDao.findAll().subList((page - 1) * count, page * count);
    }
}
