package service.crud;

import db.dao.hibernate.FineDao;
import db.dao.hibernate.impl.FineDaoImpl;
import entity.hibernate.Fine;
import util.validator.PriceValidator;
import util.validator.ValidatorException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class FineService {
    private final PriceValidator priceValidator = new PriceValidator();
    private final FineDao fineDAO = new FineDaoImpl();

    public void update(HttpServletRequest req) throws ValidatorException {
        long fineId = priceValidator.validateId(req.getParameter("id"), "Fine id must be not null", "Fine id must be a 'long': " + req.getParameter("id"));
        long orderId = priceValidator.validateId(req.getParameter("orderId"), "Order id must be not null", "Order id must be a 'long': " + req.getParameter("orderId"));
        Float price = priceValidator.validatePrice(req.getParameter("price"), false);
        String comment = priceValidator.validNotNull(req.getParameter("comment"),"Comment must be not null");

        //this method throw exception if not all params valid
        priceValidator.valid("Fine form is not correct: ");

        Fine fine = fineDAO.findById((int) fineId);;

        fine.setOrder((int) orderId);
        fine.setPrice(price);
        fine.setComment(comment);

        fineDAO.update(fine);
    }

    public void create(HttpServletRequest req) throws ValidatorException {
        long orderId = priceValidator.validateId(req.getParameter("orderId"), "Order id must be not null", "Order id must be a 'long': " + req.getParameter("orderId"));
        Float price = priceValidator.validatePrice(req.getParameter("price"), false);
        String comment = priceValidator.validNotNull(req.getParameter("comment"),"Comment must be not null");

        //this method throw exception if not all params valid
        priceValidator.valid("Fine form is not correct: ");
        Fine fine = new Fine((int) orderId, price, comment);

        fineDAO.save(fine);
    }

    public void delete(HttpServletRequest req) throws ValidatorException {
        long id = priceValidator.validateId(req.getParameter("id"), "Fine id must be not null", "Fine id must be a 'long': " + req.getParameter("id"));

        //this method throw exception if not all params valid
        priceValidator.valid("Fine form is not correct: ");

        fineDAO.delete(fineDAO.findById((int) id));
    }

    public Fine get(HttpServletRequest req) throws ValidatorException {
        long id = priceValidator.validateId(req.getParameter("id"), "Fine id must be not null", "Fine id must be a 'long': " + req.getParameter("id"));
        priceValidator.valid("Fine form is not correct: ");
        return fineDAO.findById((int) id);
    }

    public List<Fine> getAll() {
        return fineDAO.findAll();
    }
}
