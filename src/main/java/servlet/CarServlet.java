package servlet;

import service.crud.CarService;
import service.crud.ImageService;
import service.crud.PriceService;
import entity.hibernate.Car;
import entity.hibernate.Image;
import entity.Item;
import entity.hibernate.Price;
import util.PaginationUtils;
import util.validator.GeneralValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/carList")
public class CarServlet extends HttpServlet {
    private final GeneralValidator generalValidator = new GeneralValidator();

    private final CarService carTableAdmin = new CarService();
    private final PriceService priceTableAdmin = new PriceService();
    private final ImageService imageTableAdmin = new ImageService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1; // default value of page

        String reqPage = request.getParameter("page");
        if (reqPage != null) {
            if (generalValidator.isInt(reqPage)) {
                page = Integer.parseInt(reqPage);
            }
        }

        final int items = 7;
        int count = carTableAdmin.getAll().size();

        PaginationUtils pagination = new PaginationUtils();
        int[] pages = pagination.pagination(count, items);
        request.setAttribute("pages", pages);

        List<Car> cars = carTableAdmin.getPage(page, items);
        List<Item> cards = new ArrayList<>();
        for (Car car : cars) {

            List<Price> prices = priceTableAdmin.getByCar(car.getId());
            List<Image> images = imageTableAdmin.getByCar(car.getId());
            Float max = null;
            Float min = null;
            String urn = "";

            if (prices.size() > 0) {
                max = prices.get(0).getPrice();
                min = prices.get(0).getPrice();
            }
            for (Price price : prices) {
                if (max < price.getPrice()) {
                    max = price.getPrice();
                }
                if (min > price.getPrice()) {
                    min = price.getPrice();
                }
            }

            for (Image image : images) {
                if (image.getType().equalsIgnoreCase("main")) {
                    urn = image.getUrn();
                }
            }

            Item item = new Item(car, max, min, urn);
            cards.add(item);
        }

        request.setAttribute("items", cards);

        RequestDispatcher dispatcher //
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/carListView.jsp");

        dispatcher.forward(request, response);
    }
}
