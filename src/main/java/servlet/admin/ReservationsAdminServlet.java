package servlet.admin;

import entity.hibernate.Reservation;
import service.crud.ReservationService;
import util.validator.ValidatorException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/reservationsAdmin")
public class ReservationsAdminServlet extends HttpServlet {

    private final ReservationService reservationTableAdmin = new ReservationService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String event = request.getParameter("event");

        //choose event and validate date
        try {
            switch (event) {
                case"update":
                    reservationTableAdmin.update(request);
                    break;
                case "create":
                    reservationTableAdmin.create(request);
                    break;
                case "delete":
                    reservationTableAdmin.delete(request);
                    break;
                default:
            }
        } catch (ValidatorException e) {
            e.printStackTrace();
            System.out.println(e.formatDetails());

            request.setAttribute("error", e);
            doGet(request,response);
        }

        String path = request.getContextPath() + "/reservations";
        response.sendRedirect(path);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long reservationId = Long.parseLong(request.getParameter("id"));
        String event = request.getParameter("event");

        Reservation reservation = null;
        try {
            reservation = reservationTableAdmin.get(request);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }

        if (event.equalsIgnoreCase("delete")) {
            doPost(request, response);
        } else {
            request.setAttribute("reservation", reservation);
            request.setAttribute("id", reservationId);
            request.setAttribute("event", event);
            RequestDispatcher dispatcher //
                    = this.getServletContext()//
                    .getRequestDispatcher("/WEB-INF/views/admin/reservationsAdminView.jsp");
            dispatcher.forward(request, response);
        }
    }
}
