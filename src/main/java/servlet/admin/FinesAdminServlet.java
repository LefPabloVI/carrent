package servlet.admin;

import entity.hibernate.Fine;
import service.crud.FineService;
import util.validator.ValidatorException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/finesAdmin")
public class FinesAdminServlet extends HttpServlet {
    private final FineService fineTableAdmin = new FineService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String event = request.getParameter("event");

        //choose event and validate data
        try {
            switch (event) {
                case"update":
                    fineTableAdmin.update(request);
                    break;
                case "create":
                    fineTableAdmin.create(request);
                    break;
                case "delete":
                    fineTableAdmin.delete(request);
                    break;
                default:
            }
        } catch (ValidatorException e) {
            e.printStackTrace();
            System.out.println(e.formatDetails());

            request.setAttribute("error", e);
            doGet(request,response);
        }

        String path = request.getContextPath() + "/fines";
        response.sendRedirect(path);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fineId = request.getParameter("id");
        String event = request.getParameter("event");

        Fine fine = null;
        try {
            fine = fineTableAdmin.get(request);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }

        if (event.equalsIgnoreCase("delete")) {
            doPost(request, response);
        } else {
            request.setAttribute("fine", fine);
            request.setAttribute("id", fineId);
            request.setAttribute("event", event);
            RequestDispatcher dispatcher //
                    = this.getServletContext()//
                    .getRequestDispatcher("/WEB-INF/views/admin/finesAdminView.jsp");
            dispatcher.forward(request, response);
        }
    }
}
