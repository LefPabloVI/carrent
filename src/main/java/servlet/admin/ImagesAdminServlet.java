package servlet.admin;

import entity.hibernate.Image;
import service.crud.ImageService;
import util.validator.ValidatorException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/imagesAdmin")
public class ImagesAdminServlet extends HttpServlet {
    private final ImageService imageTableAdmin = new ImageService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String event = request.getParameter("event");

        //choose event and validate data
        try {
            switch (event) {
                case"update":
                    imageTableAdmin.update(request);
                    break;
                case "create":
                    imageTableAdmin.create(request);
                    break;
                case "delete":
                    imageTableAdmin.delete(request);
                    break;
                default:
            }
        } catch (ValidatorException e) {
            e.printStackTrace();
            System.out.println(e.formatDetails());

            request.setAttribute("error", e);
            doGet(request,response);
        }

        String path = request.getContextPath() + "/images";
        response.sendRedirect(path);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long imageId = Long.parseLong(request.getParameter("id"));
        String event = request.getParameter("event");

        Image image = null;
        try {
            image = imageTableAdmin.get(request);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }

        if (event.equalsIgnoreCase("delete")) {
            doPost(request, response);
        } else {
            request.setAttribute("image", image);
            request.setAttribute("id", imageId);
            request.setAttribute("event", event);
            RequestDispatcher dispatcher //
                    = this.getServletContext()//
                    .getRequestDispatcher("/WEB-INF/views/admin/imagesAdminView.jsp");
            dispatcher.forward(request, response);
        }
    }
}
