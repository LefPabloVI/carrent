package servlet.admin;

import service.crud.CarService;
import util.validator.ValidatorException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/carsAdmin")
public class CarsAdminServlet extends HttpServlet {
    private final CarService carTableAdmin = new CarService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String event = request.getParameter("event");

        //choose event and validate date
        try {
            switch (event) {
                case"update":
                    carTableAdmin.update(request);
                    break;
                case "create":
                    carTableAdmin.create(request);
                    break;
                case "delete":
                    carTableAdmin.delete(request);
                    break;
                default:
            }
        } catch (ValidatorException e) {
            e.printStackTrace();
            System.out.println(e.formatDetails());

            request.setAttribute("error", e);
            doGet(request,response);
        }

        RequestDispatcher dispatcher //
                = this.getServletContext()//
                .getRequestDispatcher("/cars");
        dispatcher.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String carId = request.getParameter("id");
        String event = request.getParameter("event");

        if (event.equalsIgnoreCase("delete")) {
            doPost(request, response);
        } else {
            try {
                request.setAttribute("car", carTableAdmin.get(request));
            } catch (ValidatorException e) {
                e.printStackTrace();
                //later past page "something went wrong"
            }
            request.setAttribute("id", carId);
            request.setAttribute("event", event);
            RequestDispatcher dispatcher //
                    = this.getServletContext()//
                    .getRequestDispatcher("/WEB-INF/views/admin/carsAdminView.jsp");
            dispatcher.forward(request, response);
        }
    }
}
