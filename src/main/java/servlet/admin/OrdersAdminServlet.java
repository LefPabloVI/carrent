package servlet.admin;

import entity.hibernate.Order;
import service.crud.OrderService;
import util.validator.ValidatorException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ordersAdmin")
public class OrdersAdminServlet extends HttpServlet {
    private final OrderService orderTableAdmin = new OrderService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String event = request.getParameter("event");

        //choose event and validate date
        try {
            switch (event) {
                case"update":
                    orderTableAdmin.update(request);
                    break;
                case "create":
                    orderTableAdmin.create(request);
                    break;
                case "delete":
                    orderTableAdmin.delete(request);
                    break;
                default:
            }
        } catch (ValidatorException e) {
            e.printStackTrace();
            System.out.println(e.formatDetails());

            request.setAttribute("error", e);
            doGet(request,response);
        }

        String path = request.getContextPath() + "/orders";
        response.sendRedirect(path);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long orderId = Long.parseLong(request.getParameter("id"));
        String event = request.getParameter("event");

        Order order = null;
        try {
            order = orderTableAdmin.get(request);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }

        if (event.equalsIgnoreCase("delete")) {
            doPost(request, response);
        } else {
            request.setAttribute("order", order);
            request.setAttribute("id", orderId);
            request.setAttribute("event", event);
            RequestDispatcher dispatcher //
                    = this.getServletContext()//
                    .getRequestDispatcher("/WEB-INF/views/admin/ordersAdminView.jsp");
            dispatcher.forward(request, response);
        }
    }
}
