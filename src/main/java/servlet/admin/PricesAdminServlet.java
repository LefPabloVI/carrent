package servlet.admin;

import entity.hibernate.Price;
import service.crud.PriceService;
import util.validator.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/pricesAdmin")
public class PricesAdminServlet extends HttpServlet {
    private final PriceService priceTableAdmin = new PriceService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String event = request.getParameter("event");

        //choose event and validate date
        try {
            switch (event) {
                case"update":
                    priceTableAdmin.update(request);
                    break;
                case "create":
                    priceTableAdmin.create(request);
                    break;
                case "delete":
                    priceTableAdmin.delete(request);
                    break;
                default:
            }
        } catch (ValidatorException e) {
            e.printStackTrace();
            System.out.println(e.formatDetails());

            request.setAttribute("error", e);
            doGet(request,response);
        }

        String path = request.getContextPath() + "/prices";
        response.sendRedirect(path);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long priceId = Long.parseLong(request.getParameter("id"));
        String event = request.getParameter("event");

        Price price = null;
        try {
            price = priceTableAdmin.get(request);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }

        if (event.equalsIgnoreCase("delete")) {
            doPost(request, response);
        } else {
            request.setAttribute("price", price);
            request.setAttribute("id", priceId);
            request.setAttribute("event", event);
            RequestDispatcher dispatcher //
                    = this.getServletContext()//
                    .getRequestDispatcher("/WEB-INF/views/admin/pricesAdminView.jsp");
            dispatcher.forward(request, response);
        }
    }
}
