package servlet.admin;

import entity.hibernate.User;
import service.crud.UserService;
import util.AppUtils;
import util.validator.ValidatorException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/usersAdmin")
public class UsersAdminServlet extends HttpServlet {
    private final UserService userTableAdmin = new UserService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String event = request.getParameter("event");

        //check row of SUPERADMIN
        User user = null;
        try {
            user = userTableAdmin.get(request);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
        User currentUser = AppUtils.getLoginedUser(request.getSession());
        if (!userTableAdmin.accessToRow(currentUser, user)) {
            RequestDispatcher dispatcher //
                    = request.getServletContext().getRequestDispatcher("/WEB-INF/views/accessDeniedView.jsp");

            dispatcher.forward(request, response);
        }

        //choose event and validate date
        try {
            switch (event) {
                case"update":
                    userTableAdmin.update(request);
                    break;
                case "create":
                    userTableAdmin.create(request);
                    break;
                case "delete":
                    userTableAdmin.delete(request);
                    break;
                default:
            }
        } catch (ValidatorException e) {
            e.printStackTrace();
            System.out.println(e.formatDetails());


            request.setAttribute("error", e);
            doGet(request,response);
        }

        String path = request.getContextPath() + "/users";
        response.sendRedirect(path);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getParameter("id");
        String event = request.getParameter("event");

        User user = null;
        try {
            user = userTableAdmin.get(request);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }

        User currentUser = AppUtils.getLoginedUser(request.getSession());
        //check row of SUPERADMIN
        if (!userTableAdmin.accessToRow(currentUser ,user)) {
            RequestDispatcher dispatcher //
                    = request.getServletContext().getRequestDispatcher("/WEB-INF/views/accessDeniedView.jsp");

            dispatcher.forward(request, response);
        }

        if (event.equalsIgnoreCase("delete")) {
            doPost(request, response);
        } else {
            request.setAttribute("user", user);
            request.setAttribute("id", userId);
            request.setAttribute("event", event);
            RequestDispatcher dispatcher //
                    = this.getServletContext()//
                    .getRequestDispatcher("/WEB-INF/views/admin/usersAdminView.jsp");
            dispatcher.forward(request, response);
        }
    }
}
