package servlet.table;

import service.crud.ReservationService;
import entity.hibernate.Reservation;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/reservations")
public class ReservationsServlet extends HttpServlet {

    private final ReservationService reservationTableAdmin = new ReservationService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Reservation> reservations = reservationTableAdmin.getAll();
        List<List<String>> objects = new ArrayList<List<String>>();
        for (Reservation reservation : reservations) {
            List<String> params = new ArrayList<String>();

            params.add(reservation.getId() + "");
            params.add(reservation.getName() + "");
            params.add(reservation.getPhone() + "");
            params.add(reservation.getEmail() + "");
            params.add(reservation.getStartDate() + "");
            params.add(reservation.getEndDate() + "");
            params.add(reservation.getTimestamp() + "");

            objects.add(params);
        }
        List<String> columns = new ArrayList<String>();

        columns.add("Id");
        columns.add("Name");
        columns.add("Phone");
        columns.add("Email");
        columns.add("Start Date");
        columns.add("End Date");
        columns.add("Timestamp");

        request.setAttribute("title", "Reservations");
        request.setAttribute("name", "Reservations");
        request.setAttribute("servlet", "/reservationsAdmin");
        request.setAttribute("columns", columns);
        request.setAttribute("objects", objects);

        RequestDispatcher dispatcher
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/template/tableView.jsp");
        dispatcher.forward(request, response);
    }
}
