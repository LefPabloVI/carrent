package servlet.table;

import service.crud.OrderService;
import entity.hibernate.Order;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/orders")
public class OrdersServlet extends HttpServlet {

    private final OrderService orderTableAdmin = new OrderService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Order> orders = orderTableAdmin.getAll();
        List<List<String>> objects = new ArrayList<List<String>>();
        for (Order order : orders) {
            List<String> params = new ArrayList<String>();

            params.add(order.getId() + "");
            params.add(order.getClient() + "");
            params.add(order.getCar() + "");
            params.add(order.getStartPeriod() + "");
            params.add(order.getEndPeriod() + "");
            params.add(order.getKilometers() + "");
            params.add(order.getPrice() + "");
            params.add(order.getStatus() + "");
            params.add(order.getUpdated() + "");

            objects.add(params);
        }
        List<String> columns = new ArrayList<String>();

        columns.add("Id");
        columns.add("Id User");
        columns.add("Id Car");
        columns.add("Start Date");
        columns.add("End Date");
        columns.add("Kilometers");
        columns.add("Price");
        columns.add("UserStatus");
        columns.add("Updated");

        request.setAttribute("title", "Orders");
        request.setAttribute("name", "Orders");
        request.setAttribute("servlet", "/ordersAdmin");
        request.setAttribute("columns", columns);
        request.setAttribute("objects", objects);

        RequestDispatcher dispatcher
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/template/tableView.jsp");
        dispatcher.forward(request, response);
    }
}
