package servlet.table;

import db.dao.hibernate.CarDao;
import db.dao.hibernate.impl.CarDaoImpl;
import entity.hibernate.Car;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/cars")
public class CarsServlet extends HttpServlet {
    private final CarDao carDao = new CarDaoImpl();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Car> cars = carDao.findAll();
        List<List<String>> objects = new ArrayList<List<String>>();
        for (Car car : cars) {
            List<String> params = new ArrayList<String>();

            params.add(car.getId() + "");
            params.add(car.getModel());
            params.add(car.getCarClass());
            params.add(car.getTransmission());
            params.add(car.getBrand());
            params.add(car.getColor());
            params.add(car.getYear());
            params.add(car.getPassengers());
            params.add(car.getDoors());
            params.add(car.getFuelConsumption());
            params.add(car.getTypeAndEngineDisplacement());
            params.add(car.getOptions());

            objects.add(params);
        }

        List<String> columns = new ArrayList<String>();

        columns.add("Id");
        columns.add("Model");
        columns.add("Car Class");
        columns.add("Body");
        columns.add("Transmission");
        columns.add("Brand");
        columns.add("Color");
        columns.add("Year");
        columns.add("Passengers");
        columns.add("Doors");
        columns.add("Fuel Consumption");
        columns.add("Type And Engine Displacement");
        columns.add("Options");

        request.setAttribute("title", "Cars");
        request.setAttribute("name", "Cars");
        request.setAttribute("servlet", "/carsAdmin");
        request.setAttribute("columns", columns);
        request.setAttribute("objects", objects);

        RequestDispatcher dispatcher
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/template/tableView.jsp");
        dispatcher.forward(request, response);
    }
}
