package servlet.table;

import entity.hibernate.Price;
import entity.enums.Period;
import service.crud.PriceService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/prices")
public class PricesServlet extends HttpServlet {

    private final PriceService priceTableAdmin = new PriceService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Price> prices = priceTableAdmin.getAll();
        List<List<String>> objects = new ArrayList<List<String>>();
        for (Price price : prices) {
            List<String> params = new ArrayList<String>();

            params.add(price.getId() + "");
            params.add(price.getIdCar() + "");
            params.add(Period.valueOf(price.getPeriod()).getText());
            params.add(price.getPrice() + "");

            objects.add(params);
        }

        List<String> columns = new ArrayList<String>();

        columns.add("Id");
        columns.add("Id Car");
        columns.add("Period");
        columns.add("Price");

        request.setAttribute("title", "Prices");
        request.setAttribute("name", "Prices");
        request.setAttribute("servlet", "/pricesAdmin");
        request.setAttribute("columns", columns);
        request.setAttribute("objects", objects);

        RequestDispatcher dispatcher
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/template/tableView.jsp");
        dispatcher.forward(request, response);
    }
}
