package servlet.table;

import entity.hibernate.User;
import service.crud.UserService;
import util.AppUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/users")
public class UsersServlet extends HttpServlet {
    private final UserService userTableAdmin = new UserService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<User> users = userTableAdmin.getAll();
        List<List<String>> objects = new ArrayList<List<String>>();
        User currentUser = AppUtils.getLoginedUser(request.getSession());
        for (User user : users) {
            List<String> params = new ArrayList<String>();

            if (!userTableAdmin.accessToRow(currentUser ,user)) {
                continue;
            }

            params.add(user.getId() + "");
            params.add(user.getName() + "");
            params.add(user.getAge() + "");
            params.add(user.getPhone() + "");
            params.add(user.getEmail() + "");
            params.add(user.getPassport() + "");
            params.add(user.getDriverLicense() + "");
            params.add(user.getDrivingExperience() + "");
            params.add(user.getEmployeeCode() + "");
            params.add(user.getPassword() + "");
            params.add(user.getStatus() + "");

            objects.add(params);
        }
        List<String> columns = new ArrayList<String>();

        columns.add("Id");
        columns.add("Name");
        columns.add("Age");
        columns.add("Phone");
        columns.add("Email");
        columns.add("Passport");
        columns.add("DriverLicense");
        columns.add("DrivingExperience");
        columns.add("EmployeeCode");
        columns.add("Password");
        columns.add("UserStatus");

        request.setAttribute("title", "Users");
        request.setAttribute("name", "Users");
        request.setAttribute("servlet", "/usersAdmin");
        request.setAttribute("columns", columns);
        request.setAttribute("objects", objects);

        RequestDispatcher dispatcher
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/template/tableView.jsp");
        dispatcher.forward(request, response);
    }
}
