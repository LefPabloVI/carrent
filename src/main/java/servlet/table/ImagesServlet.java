package servlet.table;

import service.crud.ImageService;
import entity.hibernate.Image;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/images")
public class ImagesServlet extends HttpServlet {

    private final ImageService imageTableAdmin = new ImageService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Image> images = imageTableAdmin.getAll();
        List<List<String>> objects = new ArrayList<List<String>>();
        for (Image image : images) {
            List<String> params = new ArrayList<String>();

            params.add(image.getId() + "");
            params.add(image.getEntity() + "");
            params.add(image.getElementId() + "");
            params.add(image.getType() + "");
            params.add(image.getUrn() + "");

            objects.add(params);
        }

        List<String> columns = new ArrayList<String>();

        columns.add("Id");
        columns.add("Entity");
        columns.add("Id Order");
        columns.add("Type");
        columns.add("Urn");

        request.setAttribute("title", "Images");
        request.setAttribute("name", "Images");
        request.setAttribute("servlet", "/imagesAdmin");
        request.setAttribute("columns", columns);
        request.setAttribute("objects", objects);

        RequestDispatcher dispatcher
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/template/tableView.jsp");
        dispatcher.forward(request, response);
    }
}
