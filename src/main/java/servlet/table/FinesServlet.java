package servlet.table;

import entity.hibernate.Fine;
import service.crud.FineService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/fines")
public class FinesServlet extends HttpServlet {

    private final FineService fineTableAdmin = new FineService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Fine> fines = fineTableAdmin.getAll();
        List<List<String>> objects = new ArrayList<List<String>>();
        for (Fine fine : fines) {
            List<String> params = new ArrayList<String>();

            params.add(fine.getId() + "");
            params.add(fine.getOrder() + "");
            params.add(fine.getPrice() + "");
            params.add(fine.getComment());

            objects.add(params);
        }

        List<String> columns = new ArrayList<String>();

        columns.add("Id");
        columns.add("Id Order");
        columns.add("Price");
        columns.add("Comment");

        request.setAttribute("title", "Fines");
        request.setAttribute("name", "Fines");
        request.setAttribute("servlet", "/finesAdmin");
        request.setAttribute("columns", columns);
        request.setAttribute("objects", objects);

        RequestDispatcher dispatcher
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/template/tableView.jsp");
        dispatcher.forward(request, response);
    }
}
