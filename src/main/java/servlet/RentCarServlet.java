package servlet;

import entity.hibernate.Car;
import entity.hibernate.Image;
import entity.hibernate.Price;
import service.RentCarService;
import service.crud.ReservationService;
import util.validator.ValidatorException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/rentCar")
public class RentCarServlet extends HttpServlet {
    private final RentCarService rentCarService = new RentCarService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ReservationService reservationTableAdmin = new ReservationService();

        try {
            reservationTableAdmin.create(request);
        } catch (ValidatorException e) {
            e.printStackTrace();
            System.out.println(e.formatDetails());

            request.setAttribute("error", e);
        }

        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String carId = request.getParameter("car_id");
        Car car = null;
        List<Image> images = null;
        List<Price> prices = null;
        try {
            car = rentCarService.getCar(request);
            images = rentCarService.getImages(request);
            prices = rentCarService.getPrices(request);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }

        String mainImage = null;
        for (Image image : images) {
            if (image.getType().equalsIgnoreCase("main")) {
                mainImage = image.getUrn();
                images.remove(image);
                break;
            }
        }

        request.setAttribute("car", car);
        request.setAttribute("images", images);
        request.setAttribute("mainImage", mainImage);
        request.setAttribute("prices", prices);

        RequestDispatcher dispatcher //
                = this.getServletContext()//
                .getRequestDispatcher("/WEB-INF/views/rentCarView.jsp");
        dispatcher.forward(request, response);
    }
}
