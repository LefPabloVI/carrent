package security;

import java.util.*;

public class SecurityConfig {

    public static final String ROLE_MANAGER = "MANAGER";
    public static final String ROLE_EMPLOYEE = "EMPLOYEE";
    public static final String ROLE_USER = "USER";
    public static final String ROLE_SUPERADMIN = "SUPERADMIN";

    public static final String URN_USER_INFO = "/userInfo";
    public static final String URN_CAR_ADMIN = "/carsAdmin";
    public static final String URN_ORDER_ADMIN = "/ordersAdmin";
    public static final String URN_USER_ADMIN = "/usersAdmin";
    public static final String URN_FINE_ADMIN = "/finesAdmin";
    public static final String URN_PRICE_ADMIN = "/pricesAdmin";
    public static final String URN_RESERVATION_ADMIN = "/reservationsAdmin";

    public static final String URN_CARS = "/cars";
    public static final String URN_ORDERS = "/orders";
    public static final String URN_USERS = "/users";
    public static final String URN_FINES = "/fines";
    public static final String URN_PRICES = "/prices";
    public static final String URN_RESERVATIONS = "/reservations";

    private static final List<String> admins =  new ArrayList<>();
    private static final List<String> tables = new ArrayList<>();

    static {
        admins.add(URN_CAR_ADMIN);
        admins.add(URN_ORDER_ADMIN);
        admins.add(URN_USER_ADMIN);
        admins.add(URN_FINE_ADMIN);
        admins.add(URN_PRICE_ADMIN);
        admins.add(URN_RESERVATION_ADMIN);

        tables.add(URN_CARS);
        tables.add(URN_ORDERS);
        tables.add(URN_USERS);
        tables.add(URN_FINES);
        tables.add(URN_PRICES);
        tables.add(URN_RESERVATIONS);
    }

    // String: Role
    // List<String>: urlPatterns.
    private static final Map<String, List<String>> mapConfig = new HashMap<String, List<String>>();

    static {
        init();
    }

    private static void init() {

        // Configuration for "USER" role.
        List<String> urlPatterns = new ArrayList<String>();

        urlPatterns.add(URN_USER_INFO);

        mapConfig.put(ROLE_USER, urlPatterns);

        // Configuration for "EMPLOYEE" role.
        List<String> urlPatterns1 = new ArrayList<String>();

        urlPatterns1.add(URN_USER_INFO);
        urlPatterns1.addAll(admins);
        urlPatterns1.addAll(tables);

        mapConfig.put(ROLE_EMPLOYEE, urlPatterns1);

        // Configuration for "MANAGER" role.
        List<String> urlPatterns2 = new ArrayList<String>();

        urlPatterns2.add(URN_USER_INFO);
        urlPatterns2.addAll(admins);
        urlPatterns2.addAll(tables);

        mapConfig.put(ROLE_MANAGER, urlPatterns2);

        // Configuration for "SUPERADMIN" role.
        List<String> urlPatterns3 = new ArrayList<String>();

        urlPatterns3.add(URN_USER_INFO);
        urlPatterns3.addAll(admins);
        urlPatterns3.addAll(tables);

        mapConfig.put(ROLE_SUPERADMIN, urlPatterns3);
    }

    public static Set<String> getAllAppRoles() {
        return mapConfig.keySet();
    }

    public static List<String> getUrlPatternsForRole(String role) {
        return mapConfig.get(role);
    }
}
