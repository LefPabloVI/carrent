package util;

public class PaginationUtils {
    public int[] range(int count) {
        int[] pages = new int[count];
        for (int i = 0; i < count; i++) {
            pages[i] = i + 1;
        }
        return pages;
    }

    public int[] pagination (int count, int items) {
        int allPages = (int) Math.ceil((float) count/items);
        if (allPages > 1) { // show pages only if count of pages bigger then 1
            return range(allPages);
        } else {
            return new int[0];
        }
    }
}
