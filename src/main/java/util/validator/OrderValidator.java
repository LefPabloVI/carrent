package util.validator;

import entity.enums.OrderStatus;
import util.DateUtils;

import java.time.LocalDate;

public class OrderValidator extends GeneralValidator {
    public long validateId (String id, boolean required) {
        return super.validateId(id, "Order id must be not null", "Order id must be a 'long': " + id);
    }

    public Integer validateKilometers (String kilometers, boolean required) {
        Integer num = null;
        if (!exist(kilometers)) {
            if (required) {
                details.add("Kilometers must be not null");
                return num;
            }
            return num;
        }
        if (!isInt(kilometers)) {
            details.add("Kilometers must be a integer: " + kilometers);
            return num;
        }

        num = Integer.parseInt(kilometers);

        if (!isInterval(num, 0)) {
            details.add("Kilometers must be positive: " + num);
            return num;
        }
        return num;
    }

    public String validateStatus (String status, boolean required) {
        if (!exist(status)) {
            if (required) {
                details.add("UserStatus must be not null");
                return status;
            }
            return status;
        }
        if (!isInEnum(status, OrderStatus.class)) {
            details.add("This status is not exist: " + status);
            return status;
        }
        status = OrderStatus.valueOf(status).toString();
        return  status;
    }

    public LocalDate validateStartDate (String date, boolean required) {
        LocalDate startDate = null;
        if (!exist(date)) {
            if (required) {
                details.add("Start date must be not null");
                return startDate;
            }
            return startDate;
        }
        if (!isLocalDate(date)) {
            details.add("Start date is not local date: " + date + " ( Example: 2019-11-28 or 28/11/2019)");
            return startDate;
        }
        startDate = DateUtils.toLocalDate(date);
        return startDate;
    }

    public LocalDate validateEndDate (String date, boolean required) {
        LocalDate endDate = null;
        if (!exist(date)) {
            if (required) {
                details.add("End date must be not null");
                return endDate;
            }
            return endDate;
        }
        if (!isLocalDate(date)) {
            details.add("End date is not local date: " + date + " ( Example: 2019-11-28 or 28/11/2019)");
            return endDate;
        }
        endDate = DateUtils.toLocalDate(date);
        return endDate;
    }

    public boolean validatePeriod (LocalDate start, LocalDate end) {
        if (start != null && end != null) {
            if (start.isAfter(end) || start.isEqual(end)) {
                details.add("Period is not correct: " + start + " / " + end + " (start date must be earlier than the end date)");
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
