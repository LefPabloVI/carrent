package util.validator;

import entity.enums.Role;
import entity.enums.UserStatus;

public class UserValidator extends GeneralValidator  {
    public long validateId (String id, boolean required) {
        return super.validateId(id, "", "");
    }

    public String validatePassport (String passport, boolean required) {
        if (!exist(passport)) {
            if (required) {
                details.add("Passport must be not null");
                return passport;
            }
            return passport;
        }
        if (!isPassport(passport)) {
            details.add("Passport is not correct: " + passport + " ( Example: MP6748495 )");
            return passport;
        }
        return  passport;
    }

    public String validatePassword (String password, boolean required) {
        if (!exist(password)) {
            if (required) {
                details.add("Password must be not null");
                return password;
            }
            return password;
        }
        return  password;
    }

    public Integer validateDrivingExperience (String drivingExperience, boolean required) {
        Integer num = null;
        if (!exist(drivingExperience)) {
            if (required) {
                details.add("DrivingExperience must be not null");
                return num;
            }
            return num;
        }
        if (!isInt(drivingExperience)) {
            details.add("DrivingExperience must be a integer: " + drivingExperience);
            return num;
        }

        num = Integer.parseInt(drivingExperience);

        if (!isInterval(num, 1)) {
            details.add("DrivingExperience must bigger then 0: " + drivingExperience);
            return num;
        }
        return  num;
    }

    public String validateName (String name, boolean required) {
        if (!exist(name)) {
            if (required) {
                details.add("Name must be not null");
                return name;
            }
            return name;
        }
        if (!isInterval(name.length(), 0, 100)) {
            details.add("Name must be not longer than 100: " + name);
        }
        return name;
    }

    public Integer validateAge (String age, boolean required) {
        Integer num = null;
        if (!exist(age)) {
            if (required) {
                details.add("Age must be not null");
                return num;
            }
            return num;
        }
        if (!isInt(age)) {
            details.add("Age must be a integer: " + age);
            return num;
        }
        num = Integer.parseInt(age);
        return  num;
    }

    public String validateEmail (String email, boolean required) {
        if (!exist(email)) {
            if (required) {
                details.add("Email must be not null");
                return email;
            }
            return email;
        }
        if (!isEmail(email)) {
            details.add("Email is not correct: " + email + " ( Example: vasia.puprkin@gmail.com )");
            return  email;
        }
        return  email;
    }

    public String validatePhone (String phone, boolean required) {
        if (!exist(phone)) {
            if (required) {
                details.add("Phone must be not null");
                return phone;
            }
            return phone;
        }
        if (!isPhone(phone)) {
            details.add("Phone is not correct: " + phone + " (Phone must have one of this view: +375 29 576 8833 ; 25 543 3423 ; 8044 435 2683 (spaces not taken into account))");
            return  phone;
        }
        return  phone.replace(" ", "");
    }

    public Integer validateEmployeeCode (String employeeCode, boolean required) {
        Integer num = null;
        if (!exist(employeeCode)) {
            if (required) {
                details.add("Employee code must be not null");
                return num;
            }
            return num;
        }
        if (!isInt(employeeCode)) {
            details.add("Employee code must be a integer: " + employeeCode);
            return num;
        }
        num = Integer.parseInt(employeeCode);
        if (!isInterval(num, 0)) {
            details.add("Employee code must be positive: " + num);
            return num;
        }
        return  num;
    }

    public String validateStatus (String status, boolean required) {
        if (!exist(status)) {
            if (required) {
                details.add("UserStatus must be not null");
                return status;
            }
            return status;
        }
        if (!isInEnum(status, UserStatus.class)) {
            details.add("This status not exist: " + status);
            return status;
        }
        status = UserStatus.valueOf(status).toString();
        return  status;
    }

    public String validateRole (String role, boolean required) {
        if (!exist(role)) {
            if (required) {
                details.add("Role must be not null");
                return role;
            }
            return role;
        }
        if (!isInEnum(role, Role.class)) {
            details.add("This role not exist: " + role);
            return  role;
        }
        role = Role.valueOf(role).toString();
        return  role;
    }

    public String validateDriverLicense (String driverLicense, boolean required) {
        if (!exist(driverLicense)) {
            if (required) {
                details.add("Driver license must be not null");
                return driverLicense;
            }
            return driverLicense;
        }
        return  driverLicense;
    }
}
