package util.validator;

import entity.enums.Period;

public class PriceValidator extends GeneralValidator {

    public long validateId (String id, boolean required) {
        return super.validateId(id, "Price id must be not null", "Price id must be a long: " + id);
    }

    public Float validatePrice (String price, boolean required) {
        Float num = null;
        if (!exist(price)) {
            if (required) {
                details.add("Price must be not null");
                return num;
            }
            return num;
        }
        if (!isFloat(price)) {
            details.add("Price must be a float: " + price);
            return num;
        }

        num = Float.parseFloat(price);

        if (!isInterval(num, 0)) {
            details.add("Price must be positive: " + num);
            return num;
        }
        return num;
    }

    public String validatePeriod (String period, boolean required) {
        if (!exist(period)) {
            if (required) {
                details.add("Period must be not null");
                return period;
            }
            return period;
        }
        if (!isInEnum(period, Period.class)) {
            details.add("This period is not exist: " + period);
            return period;
        }
        period = Period.valueOf(period).toString();
        return  period;
    }
}
