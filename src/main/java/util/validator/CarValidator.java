package util.validator;

import entity.enums.CarBody;
import entity.enums.CarClass;

public class CarValidator extends GeneralValidator {
    public long validateId (String id, boolean required) {
        return super.validateId(id, "Car id must not be null", "Car id is not longer: " + id);
    }

    public String validateCarClass (String carClass, boolean required) {
        if (!exist(carClass)) {
            if (required) {
                details.add("Car class must be not null");
                return carClass;
            }
            return carClass;
        }
        if (!isInEnum(carClass, CarClass.class)) {
            details.add("This car class is not exist: " + carClass);
            return carClass;
        }
        return  carClass;
    }

    public String validateBody (String body, boolean required) {
        if (!exist(body)) {
            if (required) {
                details.add("Car body must be not null");
                return body;
            }
            return body;
        }
        if (!isInEnum(body, CarBody.class)) {
            details.add("This car body is not exist: " + body);
            return body;
        }
        body = CarBody.valueOf(body).toString();
        return  body;
    }

    public String validateTransmission (String transmission, boolean required) {
        if (!exist(transmission)) {
            if (required) {
                details.add("Transmission must be not null");
                return transmission;
            }
            return transmission;
        }
        return  transmission;
    }

    public String validateBrand (String brand, boolean required) {
        if (!exist(brand)) {
            if (required) {
                details.add("Brand must be not null");
                return brand;
            }
            return brand;
        }
        return  brand;
    }

    public String validateModel (String model, boolean required) {
        if (!exist(model)) {
            if (required) {
                details.add("Model must be not null");
                return model;
            }
            return model;
        }
        return  model;
    }

    public String validateColor (String color, boolean required) {
        if (!exist(color)) {
            if (required) {
                details.add("Color must be not null");
                return color;
            }
            return color;
        }
        return  color;
    }

    public String validateYear (String year, boolean required) {
        if (!exist(year)) {
            if (required) {
                details.add("Year must be not null");
                return year;
            }
            return year;
        }
        return  year;
    }

    public String validatePassengers (String passengers, boolean required) {
        if (!exist(passengers)) {
            if (required) {
                details.add("Passengers must be not null");
                return passengers;
            }
            return passengers;
        }
        return  passengers;
    }

    public String validateDoors (String doors, boolean required) {
        if (!exist(doors)) {
            if (required) {
                details.add("Doors must be not null");
                return doors;
            }
            return doors;
        }
        return  doors;
    }

    public String validateFuelConsumption (String fuelConsumption, boolean required) {
        if (!exist(fuelConsumption)) {
            if (required) {
                details.add("Fuel consumption must be not null");
                return fuelConsumption;
            }
            return fuelConsumption;
        }
        return  fuelConsumption;
    }

    public String validateTypeAndEngineDisplacement (String typeAndEngineDisplacement, boolean required) {
        if (!exist(typeAndEngineDisplacement)) {
            if (required) {
                details.add("Type and Engine Displacement must be not null");
                return typeAndEngineDisplacement;
            }
            return typeAndEngineDisplacement;
        }
        return  typeAndEngineDisplacement;
    }

    public String validateOptions (String options, boolean required) {
        if (!exist(options)) {
            if (required) {
                details.add("Options must be not null");
                return options;
            }
            return options;
        }
        return  options;
    }
}
