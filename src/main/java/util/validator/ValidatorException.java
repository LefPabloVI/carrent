package util.validator;

import java.util.List;

public class ValidatorException extends Exception {
    private List<String> details;
    public List<String> getDetails () {
        return details;
    }

    public String formatDetails() {
        String result = "";
        for (String detail : details) {
            result += "\\r\\n -- " + detail;
        }
        return result;
    }

    public ValidatorException(String message, List<String> details) {
        super(message);
        this.details = details;
    }
}
