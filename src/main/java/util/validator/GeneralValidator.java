package util.validator;

import db.dao.hibernate.Dao;
import util.DateUtils;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class GeneralValidator {
    protected List<String> details = new ArrayList<String>();

    public void mergeDetails(GeneralValidator validator) {
        this.details.addAll(validator.details);
    }

    public boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

    public void clean() {
        details.clear();
    }

    public boolean rowExist(Dao entity, long id) {
        if(entity.findById((int)id) != null) {
            return true;
        }
        return false;
    }

    public boolean isLong(String str) {
        try {
            Long.parseLong(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

    public boolean isFloat(String str) {
        try {
            Float.parseFloat(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

    public boolean isInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

    public boolean isLocalDate(String str) {
        if (DateUtils.toLocalDate(str) != null) {
            return true;
        }
        return false;
    }

    public boolean isPhone(String phone) {
        String regex1 = "\\d{9}";
        String regex2 = "(\\+*)375\\d{9}";
        String regex3 = "80\\d{9}";

        phone = phone.replace(" ", "");
        if (phone.matches(regex1) || phone.matches(regex2) || phone.matches(regex3)) {
            return true;
        }
        return false;
    }

    public boolean isPassport(String passport) {
        String regex = "[A-Z]{2}\\d{7}";
        if (passport.matches(regex)) {
            return true;
        }
        return  false;
    }

    public boolean isCorrectPeriod(LocalDate start, LocalDate end) {
        if (start.isBefore(end) && !start.isEqual(end)) {
            return true;
        }
        return false;
    }

    public boolean isEmail(String email) {
        String regex = "(.*)@(.*)";
        if (email.matches(regex)) {
            return true;
        }
        return  false;
    }

    public boolean isInterval(double val, double start, double end) {
        if (val >= start && val<=end) {
            return true;
        }
        return  false;
    }

    public boolean isInterval(double val, double start) {
        if (val >= start) {
            return true;
        }
        return  false;
    }

    public boolean exist(String str) {
        if (str != null && str != "") {
            return true;
        }
        return false;
    }

    public <E extends Enum<E>> boolean isInEnum(String str, Class<E> clazz) {
        try {
            Enum.valueOf(clazz, str);
            return true;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return false;
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }

    public long validateId(String str, String nullException, String longException) {
        long id = 0;
        if (!exist(str)) {
            details.add(nullException);
            return id;
        }
        if (!isLong(str)) {
            details.add(longException);
            return id;
        }
        id = Long.parseLong(str);
        return  id;
    }

    public String validNotNull(String str, String error) {
        if (!exist(str)) {
            details.add(error);
        }
        return str;
    }

    public void valid(String message) throws ValidatorException {
        List<String> errors = new ArrayList<String>();
        errors.addAll(details);
        details.clear();
        if (errors.size() > 0) {
            throw new ValidatorException(message, errors);
        }
    }

    public void validAccess(HttpServletRequest request, boolean inverse, String... roles) {
        if (inverse) {
            for (String role : roles) {
                if (request.isUserInRole(role)) {
                    details.add("Access denied. You don't have permission for this action");
                    return;
                }
            }
        } else {
            for (String role : roles) {
                if (request.isUserInRole(role)) {
                    return;
                }
            }
            details.add("Access denied. You don't have permission for this action");
        }
    }

    public void validAccess(HttpServletRequest request, String... roles) {
        validAccess(request, false, roles);
    }
}
