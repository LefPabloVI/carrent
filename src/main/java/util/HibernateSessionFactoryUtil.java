package util;

import entity.hibernate.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateSessionFactoryUtil {
    private static SessionFactory sessionFactory;

    private HibernateSessionFactoryUtil() {
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration().configure()
                        .addAnnotatedClass(User.class)
                        .addAnnotatedClass(Car.class)
                        .addAnnotatedClass(Fine.class)
                        .addAnnotatedClass(Order.class)
                        .addAnnotatedClass(Reservation.class)
                        .addAnnotatedClass(Image.class)
                        .addAnnotatedClass(Price.class);
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sessionFactory;
    }
}
