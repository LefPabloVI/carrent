package util;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateUtils {
    public static LocalDate toLocalDate(String source) {
        //parse string date to SQL Date format
        LocalDate date = null;
        if (source != null) {
            try {
                date = LocalDate.parse(source);
            } catch (DateTimeParseException e) {
                date = parseV1(source);
            }
        }
        return date;
    }

    public static LocalDate toLocalDate(Date source) {
        LocalDate date = null;
        if (source != null) {
            date = source.toLocalDate();
        }
        return date;
    }

    public static LocalDateTime toLocalDateTime(Timestamp timestamp) {
        //parse string date to SQL Date format
        LocalDateTime date = null;
        if (timestamp != null) {
            date = timestamp.toLocalDateTime();
        }
        return date;
    }

    private static LocalDate parseV1(String source) {
        LocalDate date = null;
        String dataFormat = "dd/MM/yyyy";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dataFormat);
        try {
            date = LocalDate.parse(source, formatter);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
