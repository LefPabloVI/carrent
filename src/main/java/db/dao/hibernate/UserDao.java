package db.dao.hibernate;

import entity.hibernate.User;

import java.util.List;

public interface UserDao {
    User findById(int id);
    User findByEmailPassword(String email, String password);
    void save(User user);
    void update(User user);
    void delete(User user);
    List<User> findAll();
}
