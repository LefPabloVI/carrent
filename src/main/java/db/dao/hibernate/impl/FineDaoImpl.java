package db.dao.hibernate.impl;

import db.dao.hibernate.Dao;
import db.dao.hibernate.FineDao;
import entity.hibernate.Fine;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateSessionFactoryUtil;

import java.util.List;

public class FineDaoImpl implements FineDao, Dao<Fine> {

    private static final String GET_ALL = "From Fine";

    @Override
    public Fine findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        Fine fine = session.get(Fine.class ,id);
        tx1.commit();
        session.close();
        return fine;
    }

    @Override
    public void save(Fine fine) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(fine);
        tx1.commit();
        session.close();
    }

    @Override
    public void update(Fine fine) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(fine);
        tx1.commit();
        session.close();
    }

    @Override
    public void delete(Fine fine) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(fine);
        tx1.commit();
        session.close();
    }

    @Override
    public List<Fine> findAll() {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(GET_ALL, Fine.class).list();
    }
}
