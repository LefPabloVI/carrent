package db.dao.hibernate.impl;

import db.dao.hibernate.Dao;
import db.dao.hibernate.PriceDao;
import entity.hibernate.Price;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import util.HibernateSessionFactoryUtil;

import java.util.ArrayList;
import java.util.List;

public class PriceDaoImpl implements PriceDao, Dao<Price> {

    private static final String GET_ALL = "From Price";
    private static final String GET_BY_CAR = "From Price P where P.idCar = :carId";

    @Override
    public Price findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Price price = session.get(Price.class, id);
        session.getTransaction().commit();
        session.close();
        return price;
    }

    @Override
    public void save(Price price) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(price);
        tx1.commit();
        session.close();
    }

    @Override
    public void update(Price price) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(price);
        tx1.commit();
        session.close();
    }

    @Override
    public void delete(Price price) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(price);
        tx1.commit();
        session.close();
    }

    @Override
    public List<Price> findAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<Price> prices = new ArrayList<>();
        try {
            prices = session.createQuery(GET_ALL, Price.class).list();
        } catch (Exception e) {

        }
        session.getTransaction().commit();
        session.close();
        return prices;
    }

    @Override
    public List<Price> findByCar(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<Price> prices = new ArrayList<>();
        try {
            prices = session.createQuery(GET_BY_CAR, Price.class)
                    .setParameter("carId", id).list();
        } catch (Exception e) {

        }
        session.getTransaction().commit();
        session.close();
        return prices;
    }
}
