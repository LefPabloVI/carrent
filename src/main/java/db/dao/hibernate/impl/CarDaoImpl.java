package db.dao.hibernate.impl;

import db.dao.hibernate.CarDao;
import db.dao.hibernate.Dao;
import entity.hibernate.Car;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateSessionFactoryUtil;

import java.util.ArrayList;
import java.util.List;

public class CarDaoImpl implements CarDao, Dao<Car> {

    private static final String GET_ALL = "From Car";

    public Car findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Car car = session.get(Car.class, id);
        session.getTransaction().commit();
        session.close();
        return car;    }

    public void save(Car car) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(car);
        tx1.commit();
        session.close();
    }

    public void update(Car car) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(car);
        tx1.commit();
        session.close();
    }

    public void delete(Car car) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(car);
        tx1.commit();
        session.close();
    }

    public List<Car> findAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<Car> cars = new ArrayList<>();
        try {
            cars = session.createQuery(GET_ALL, Car.class).list();
        } catch (Exception e) {

        }
        session.getTransaction().commit();
        session.close();
        return cars;
    }
}
