package db.dao.hibernate.impl;

import db.dao.hibernate.Dao;
import db.dao.hibernate.UserDao;
import entity.hibernate.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import util.HibernateSessionFactoryUtil;

import javax.persistence.NoResultException;
import java.util.List;

public class UserDaoImpl implements UserDao, Dao<User> {

    private static final String GET_ALL = "From User";
    private static final String GET_BY_EMAIL_PASSWORD = "FROM User where email = :email and password = :password";

    public User findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(User.class, id);
    }

    public User findByEmailPassword(String email, String password) {
        Query query = HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(GET_BY_EMAIL_PASSWORD);
        query.setParameter("email", email);
        query.setParameter("password", password);

        User user = null;
        try {
            user = (User) query.getSingleResult();
        } catch (NoResultException e) {
            //method must return null if not found entity
        }
        return user;
    }

    public void save(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(user);
        tx1.commit();
        session.close();
    }

    public void update(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(user);
        tx1.commit();
        session.close();
    }

    public void delete(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(user);
        tx1.commit();
        session.close();
    }

    public List<User> findAll() {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(GET_ALL, User.class).list();
    }
}
