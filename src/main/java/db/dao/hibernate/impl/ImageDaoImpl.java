package db.dao.hibernate.impl;

import db.dao.hibernate.Dao;
import db.dao.hibernate.ImageDao;
import entity.hibernate.Image;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateSessionFactoryUtil;

import java.util.ArrayList;
import java.util.List;

public class ImageDaoImpl implements ImageDao, Dao<Image> {

    private static final String GET_ALL = "From Image";
    private static final String GET_BY_CAR = "From Image where elementId = :carId";

    @Override
    public Image findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Image image = session.get(Image.class, id);
        session.getTransaction().commit();
        session.close();
        return image;
    }

    @Override
    public void save(Image image) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(image);
        tx1.commit();
        session.close();
    }

    @Override
    public void update(Image image) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(image);
        tx1.commit();
        session.close();
    }

    @Override
    public void delete(Image image) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(image);
        tx1.commit();
        session.close();
    }

    @Override
    public List<Image> findAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<Image> images = new ArrayList<>();
        try {
            images = session.createQuery(GET_ALL, Image.class).list();
        } catch (Exception e) {

        }
        session.getTransaction().commit();
        session.close();
        return images;
    }

    @Override
    public List<Image> findByCar(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<Image> images = new ArrayList<>();
        try {
            images = session.createQuery(GET_BY_CAR, Image.class)
                    .setParameter("carId", id).list();
        } catch (Exception e) {

        }
        session.getTransaction().commit();
        session.close();
        return images;
    }
}
