package db.dao.hibernate.impl;

import db.dao.hibernate.Dao;
import db.dao.hibernate.ReservationDao;
import entity.hibernate.Reservation;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateSessionFactoryUtil;

import java.util.List;

public class ReservationDaoImpl implements ReservationDao, Dao<Reservation> {

    private static final String GET_ALL = "From Reservation";

    @Override
    public Reservation findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Reservation.class, id);
    }

    @Override
    public void save(Reservation reservation) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(reservation);
        tx1.commit();
        session.close();
    }

    @Override
    public void update(Reservation reservation) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(reservation);
        tx1.commit();
        session.close();
    }

    @Override
    public void delete(Reservation reservation) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(reservation);
        tx1.commit();
        session.close();
    }

    @Override
    public List<Reservation> findAll() {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(GET_ALL, Reservation.class).list();
    }
}
