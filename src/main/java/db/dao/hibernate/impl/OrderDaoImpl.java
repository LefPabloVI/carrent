package db.dao.hibernate.impl;

import db.dao.hibernate.Dao;
import db.dao.hibernate.OrderDao;
import entity.hibernate.Order;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateSessionFactoryUtil;

import java.util.List;

public class OrderDaoImpl implements OrderDao, Dao<Order> {

    private static final String GET_ALL = "From Order";

    @Override
    public Order findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Order.class, id);
    }

    @Override
    public void save(Order order) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(order);
        tx1.commit();
        session.close();
    }

    @Override
    public void update(Order order) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(order);
        tx1.commit();
        session.close();
    }

    @Override
    public void delete(Order order) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(order);
        tx1.commit();
        session.close();
    }

    @Override
    public List<Order> findAll() {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(GET_ALL, Order.class).list();
    }
}
