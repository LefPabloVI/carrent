package db.dao.hibernate;

import entity.hibernate.Car;

import java.util.List;

public interface CarDao {
    Car findById(int id);
    void save(Car car);
    void update(Car car);
    void delete(Car car);
    List<Car> findAll();
}
