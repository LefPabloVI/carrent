package db.dao.hibernate;

import entity.hibernate.Price;

import java.util.List;

public interface PriceDao {
    Price findById(int id);
    List<Price> findByCar(int id);
    void save(Price price);
    void update(Price price);
    void delete(Price price);
    List<Price> findAll();
}
