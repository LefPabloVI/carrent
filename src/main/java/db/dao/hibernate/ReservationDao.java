package db.dao.hibernate;

import entity.hibernate.Reservation;

import java.util.List;

public interface ReservationDao {
    Reservation findById(int id);
    void save(Reservation reservation);
    void update(Reservation reservation);
    void delete(Reservation reservation);
    List<Reservation> findAll();
}
