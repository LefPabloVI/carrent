package db.dao.hibernate;

import entity.hibernate.Image;
import entity.hibernate.Price;

import java.util.List;

public interface ImageDao {
    Image findById(int id);
    List<Image> findByCar(int id);
    void save(Image image);
    void update(Image image);
    void delete(Image image);
    List<Image> findAll();
}
