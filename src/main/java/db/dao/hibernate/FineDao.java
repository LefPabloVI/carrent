package db.dao.hibernate;

import entity.hibernate.Fine;

import java.util.List;

public interface FineDao {
    Fine findById(int id);
    void save(Fine fine);
    void update(Fine fine);
    void delete(Fine fine);
    List<Fine> findAll();
}
