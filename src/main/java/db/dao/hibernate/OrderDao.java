package db.dao.hibernate;

import entity.hibernate.Order;

import java.util.List;

public interface OrderDao {
    Order findById(int id);
    void save(Order order);
    void update(Order order);
    void delete(Order order);
    List<Order> findAll();
}
