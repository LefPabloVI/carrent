<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 03.12.2019
  Time: 13:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Cars</title>
    <link rel="stylesheet" href="<c:url value="/css/carList.css"/>"/>
</head>
<jsp:include page="template/_menu.jsp"></jsp:include>
<h3>Cars</h3>
<body>
<c:forEach var="item" items="${items}">
    <div style="padding: 10px; margin-bottom: 20px;">
        <a href="<c:url value="/rentCar?car_id=${item.car.id}"/>">
            <h3>${item.car.model}</h3>
            <img src="<c:url value="${item.urn}"/>"/>
        </a>
        <p style="margin: 3px">КПП: ${item.car.transmission}</p>
        <p style="margin: 3px">год выпуска: ${item.car.year}</p>
        <p style="margin: 3px">расход топлива (город/трасса): ${item.car.fuelConsumption}</p>
        <c:if test="${item.max != null && item.min != null}">
            <p style="margin: 3px">Цена: от ${item.min} до ${item.max}</p>
        </c:if>
    </div>
</c:forEach>
<c:forEach var="pg" items="${pages}">
    <a href="<c:url value="/carList?page=${pg}"/>">${pg} </a>
</c:forEach>
</body>
</html>
