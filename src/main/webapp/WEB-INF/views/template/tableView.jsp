<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 06.12.2019
  Time: 13:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${title}</title>
</head>
<body>
<jsp:include page="_menu.jsp"></jsp:include>

<h3>${name}</h3>
<table>
    <thead>
    <c:forEach var="column" items="${columns}">
        <th>${column}</th>
    </c:forEach>
    </thead>
    <c:forEach var="params" items="${objects}">
        <tr>
        <c:forEach var="parame" items="${params}">
            <td>${parame}</td>
        </c:forEach>
            <td><a href="<c:url value="${servlet}?event=update&id=${params.get(0)}" /> ">Update</a></td>
            <td><a href="<c:url value="${servlet}?event=delete&id=${params.get(0)}" /> ">Delete</a></td>
        </tr>
    </c:forEach>
</table>
<a href="<c:url value="${servlet}?event=create&id=0"/>">Create</a>
</body>
</html>