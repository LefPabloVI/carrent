<%@ page import="util.validator.ValidatorException" %><%--
  Created by IntelliJ IDEA.
  User: User
  Date: 05.12.2019
  Time: 13:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String result = "";
    ValidatorException e = (ValidatorException) request.getAttribute("error");
    if (e != null) {
        result = "alert(\"" + e.getMessage() + " " + e.formatDetails() + "\");";
    }
%>
<script>
    window.onload = function() {
        <%= result%>
    };
</script>
