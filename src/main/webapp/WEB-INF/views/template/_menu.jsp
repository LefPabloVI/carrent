<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 02.12.2019
  Time: 17:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${loginedUser == null}">
    <a href="${pageContext.request.contextPath}/login">
        Login
    </a>
    ||
    <a href="${pageContext.request.contextPath}/logout">
        Logout
    </a>
    ||
    <a href="${pageContext.request.contextPath}/carList">
        Rent
    </a>
</c:if>
<c:if test="${loginedUser.role == 'SUPERADMIN'}">
    <a href="${pageContext.request.contextPath}/userInfo">
        User Info
    </a>
    ||
    <a href="${pageContext.request.contextPath}/login">
        Login
    </a>
    ||
    <a href="${pageContext.request.contextPath}/logout">
        Logout
    </a>
    ||
    <a href="${pageContext.request.contextPath}/carList">
        Rent
    </a>
    ||
    <a href="${pageContext.request.contextPath}/orders">
        Orders
    </a>
    ||
    <a href="${pageContext.request.contextPath}/reservations">
        Reservations
    </a>
    ||
    <a href="${pageContext.request.contextPath}/users">
        Users
    </a>
    ||
    <a href="${pageContext.request.contextPath}/cars">
        Cars
    </a>
    ||
    <a href="${pageContext.request.contextPath}/fines">
        Fines
    </a>
    ||
    <a href="${pageContext.request.contextPath}/prices">
        Prices
    </a>
    ||
    <a href="${pageContext.request.contextPath}/images">
        Images
    </a>
</c:if>
<c:if test="${loginedUser.role == 'MANAGER'}">
    <a href="${pageContext.request.contextPath}/userInfo">
        User Info
    </a>
    ||
    <a href="${pageContext.request.contextPath}/login">
        Login
    </a>
    ||
    <a href="${pageContext.request.contextPath}/logout">
        Logout
    </a>
    ||
    <a href="${pageContext.request.contextPath}/carList">
        Rent
    </a>
    ||
    <a href="${pageContext.request.contextPath}/orders">
        Orders
    </a>
    ||
    <a href="${pageContext.request.contextPath}/reservations">
        Reservations
    </a>
    ||
    <a href="${pageContext.request.contextPath}/users">
        Users
    </a>
    ||
    <a href="${pageContext.request.contextPath}/cars">
        Cars
    </a>
    ||
    <a href="${pageContext.request.contextPath}/fines">
        Fines
    </a>
    ||
    <a href="${pageContext.request.contextPath}/prices">
        Prices
    </a>
    ||
    <a href="${pageContext.request.contextPath}/images">
        Images
    </a>
</c:if>
<c:if test="${loginedUser.role == 'EMPLOYEE'}">
    <a href="${pageContext.request.contextPath}/userInfo">
        User Info
    </a>
    ||
    <a href="${pageContext.request.contextPath}/login">
        Login
    </a>
    ||
    <a href="${pageContext.request.contextPath}/logout">
        Logout
    </a>
    ||
    <a href="${pageContext.request.contextPath}/carList">
        Rent
    </a>
    ||
    <a href="${pageContext.request.contextPath}/orders">
        Orders
    </a>
    ||
    <a href="${pageContext.request.contextPath}/reservations">
        Reservations
    </a>
    ||
    <a href="${pageContext.request.contextPath}/users">
        Users
    </a>
    ||
    <a href="${pageContext.request.contextPath}/cars">
        Cars
    </a>
    ||
    <a href="${pageContext.request.contextPath}/fines">
        Fines
    </a>
    ||
    <a href="${pageContext.request.contextPath}/prices">
        Prices
    </a>
    ||
    <a href="${pageContext.request.contextPath}/images">
        Images
    </a>
</c:if>
&nbsp;
<%--<span style="color:red">[ ${loginedUser.userName} ]</span>--%>
<span style="color:red">[ ${loginedUser.email} ]</span>
