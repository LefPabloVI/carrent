<%@ page import="entity.hibernate.Car" %>
<%@ page import="entity.enums.Period" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 03.12.2019
  Time: 13:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Rent Car</title>
    <link rel="stylesheet" href="<c:url value="/css/rentCar.css"/>"/>
</head>
<%
    Car car = (Car) request.getAttribute("car");
    if (car != null && car.getOptions() != null) {
        request.setAttribute("options", car.getOptions().split("\\|"));
    }
%>
<body>
<jsp:include page="template/_menu.jsp"></jsp:include>
<h3>Rent Car online</h3>
<form method="POST" action="${pageContext.request.contextPath}/rentCar">
    <input type="hidden" name="redirectId" value="${param.redirectId}" />
    <input type="hidden" name="car_id" value="<%=request.getParameter("car_id")%>" />
    <table border="0">
        <tr>
            <td colspan ="2">
                <img id="mainImage" src="<c:url value="${mainImage}"/>" width="100%"/>
            </td>
        </tr>
        <c:if test="${images.size() > 0}">
            <tr>
                <td colspan ="2">
                    <c:forEach var="image" items="${images}">
                        <img src="<c:url value="${image.urn}"/>" width="${100/images.size() - 1}%"/>
                    </c:forEach>
                </td>
            </tr>
        </c:if>
        <tr>
            <td>Characteristics</td>
            <td>
                <h2>${car.model}</h2>
                <ul>
                    <li>class: ${car.carClass}</li>
                    <li>body: ${car.body}</li>
                    <li>brand: ${car.brand}</li>
                    <li>color: ${car.color}</li>
                    <li>year: ${car.year}</li>
                    <li>passengers: ${car.passengers}</li>
                    <li>doors: ${car.doors}</li>
                    <li>fuel consumption: ${car.fuelConsumption}</li>
                    <li>type and engine displacement: ${car.typeAndEngineDisplacement}</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>Options</td>
            <td>
                <ul>
                    <c:forEach var="option" items="${options}">
                        <li><c:out value="${option}" /></li>
                    </c:forEach>
                </ul>
            </td>
        </tr>
        <c:if test="${prices.size() > 0}">
            <tr>
                <td>Prices</td>
                <td>
                    <ul>
                        <c:forEach var="price" items="${prices}">
                            <li>Period: ${Period.valueOf(price.period).getText()} | Price: ${price.price}</li>
                        </c:forEach>
                    </ul>
                </td>
            </tr>
        </c:if>

        <tr>
            <td>Name</td>
            <td><input type="text" name="name" required/> </td>
        </tr>
        <tr>
            <td>Email</td>
            <td><input type="text" name="email" required/> </td>
        </tr>
        <tr>
            <td>Phone</td>
            <td><input type="text" name="phone" required/> </td>
        </tr>
        <tr>
            <td>Start date</td>
            <td><input type="date" name="start" required/> </td>
        </tr>
        <tr>
            <td>End date</td>
            <td><input type="date" name="end" required/> </td>
        </tr>

        <tr>
            <td colspan ="2">
                <input type="submit" value= "Submit" />
            </td>
        </tr>
    </table>
</form>
<jsp:include page="template/_error.jsp"></jsp:include>
</body>
</html>
