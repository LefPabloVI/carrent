<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %><%--
  Created by IntelliJ IDEA.
  User: User
  Date: 04.12.2019
  Time: 11:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<%
    Map<String, String> cars = new HashMap<String, String>();
    Map<String, String> carClasses = new HashMap<String, String>();

    cars.put("MICRO_COMPACT_CAR", "micro compact car");
    cars.put("LANDAU", "landau");
    cars.put("VAN", "van");
    cars.put("MINIVAN", "minivan");
    cars.put("CABRIOLET", "cabriolet");
    cars.put("LIMOUSINE", "limousine");
    cars.put("PICKUP", "pickup");
    cars.put("CROSSOVER", "crossover");
    cars.put("SEDAN", "sedan");
    cars.put("SUV", "sport-utility vehicle");

    carClasses.put("LUX", "luxury class");
    carClasses.put("ECO", "economy class");
    carClasses.put("MID", "middle class");
    carClasses.put("EXC", "exclusive class");

    request.setAttribute("cars", cars);
    request.setAttribute("carClasses", carClasses);
%>
<body>
<jsp:include page="../template/_menu.jsp"></jsp:include>
<h3>Cars Admin</h3>
<h2>${event}</h2>
<form method="POST" action="${pageContext.request.contextPath}/carsAdmin">
    <input type="hidden" name="id" value="${id}" />
    <input type="hidden" name="event" value="${event}" />
    <table border="0">
        <tr>
            <td>Car Class</td>
            <td>
                <select name="carClass" id="carClass">
                    <c:forEach var="row" items="${carClasses.keySet()}">
                        <c:if test="${row == car.carClass}">
                            <option selected="selected" value="${row}">${carClasses.get(row)}</option>
                        </c:if>
                        <c:if test="${row != car.carClass}">
                            <option value="${row}">${carClasses.get(row)}</option>
                        </c:if>
                    </c:forEach>
                </select>
<%--                <input type="text" name="carClass" value="${car.carClass}"/> --%>
            </td>
        </tr>
        <tr>
            <td>Model</td>
            <td><input type="text" name="model" value="${car.model}"/> </td>
        </tr>
        <tr>
            <td>Body</td>
            <td>
                <select name="body" id="body">
                    <c:forEach var="row" items="${cars.keySet()}">
                        <c:if test="${row == car.body}">
                            <option selected="selected" value="${row}">${cars.get(row)}</option>
                        </c:if>
                        <c:if test="${row != car.body}">
                            <option value="${row}">${cars.get(row)}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <td>Transmission</td>
            <td><input type="text" name="transmission" value="${car.transmission}"/> </td>
        </tr>
        <tr>
            <td>Brand</td>
            <td><input type="text" name="brand" value="${car.brand}"/> </td>
        </tr>
        <tr>
            <td>Color</td>
            <td><input type="text" name="color" value="${car.color}"/> </td>
        </tr>
        <tr>
            <td>Year</td>
            <td><input type="text" name="year" value="${car.year}"/> </td>
        </tr>
        <tr>
            <td>Passengers</td>
            <td><input type="text" name="passengers" value="${car.passengers}"/> </td>
        </tr>
        <tr>
            <td>Doors</td>
            <td><input type="text" name="doors" value="${car.doors}"/> </td>
        </tr>
        <tr>
            <td>Fuel Consumption</td>
            <td><input type="text" name="fuelConsumption" value="${car.fuelConsumption}"/> </td>
        </tr>
        <tr>
            <td>Type And Engine Displacement</td>
            <td><input type="text" name="typeAndEngineDisplacement" value="${car.typeAndEngineDisplacement}"/> </td>
        </tr>
        <tr>
            <td>Options</td>
            <td><input type="text" name="options" value="${car.options}"/> </td>
        </tr>

        <tr>
            <td colspan ="2">
                <input type="submit" value= "Submit"/>
                <a href="${pageContext.request.contextPath}/cars">Cancel</a>
            </td>
        </tr>
    </table>
</form>
<jsp:include page="../template/_error.jsp"></jsp:include>
</body>
</html>
