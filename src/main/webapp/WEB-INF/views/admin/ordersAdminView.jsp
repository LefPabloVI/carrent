<%@ page import="entity.hibernate.Order" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: User
  Date: 04.12.2019
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Orders</title>
</head>

<%
    List<String> orders = new ArrayList<String>();
    List<String> values = new ArrayList<String>();
    Order order = (Order) request.getAttribute("order");
    orders.add("reservation");
    orders.add("closed");
    orders.add("performed");
    orders.add("framed");
    orders.add("completes");

    values.add("RESERVATION");
    values.add("CLOSED");
    values.add("PERFORMED");
    values.add("FRAMED");
    values.add("COMPLETES");

    String selectList = "";
    String selected;
    for (int i = 0; i < orders.size(); i++) {
        selected = "";
        if (order != null && values.get(i).equalsIgnoreCase(order.getStatus())) {
            selected = "selected=\"selected\"";
        }
        selectList += "<option " + selected + " value=\"" + values.get(i) + "\">" + orders.get(i) + "</option>\n";
    }
%>

<body>
<jsp:include page="../template/_menu.jsp"></jsp:include>
<h3>Orders Admin</h3>
<h2>${event}</h2>
<form method="POST" action="${pageContext.request.contextPath}/ordersAdmin">
    <input type="hidden" name="id" value="${id}" />
    <input type="hidden" name="event" value="${event}" />
    <table border="0">
        <tr>
            <td>Client Id</td>
            <td><input type="text" name="user" value="${order.client}"/> </td>
        </tr>
        <tr>
            <td>Car Id</td>
            <td><input type="text" name="car" value="${order.car}"/> </td>
        </tr>
        <tr>
            <td>Start Date</td>
            <td><input type="date" name="start" value="${order.startPeriod}"/> </td>
        </tr>
        <tr>
            <td>End Date</td>
            <td><input type="date" name="end" value="${order.endPeriod}"/> </td>
        </tr>
        <tr>
            <td>Kilometers</td>
            <td><input type="text" name="kilometers" value="${order.kilometers}"/> </td>
        </tr>
        <tr>
            <td>Price</td>
            <td><input type="text" name="price" value="${order.price}"/> </td>
        </tr>
        <tr>
            <td>Status</td>
            <td>
                <select name="status" id="status">
                    <%= selectList%>
                </select>
            </td>
        </tr>

        <tr>
            <td colspan ="2">
                <input type="submit" value= "Submit"/>
                <a href="${pageContext.request.contextPath}/orders">Cancel</a>
            </td>
        </tr>
    </table>
</form>
<jsp:include page="../template/_error.jsp"></jsp:include>
</body>
</html>
