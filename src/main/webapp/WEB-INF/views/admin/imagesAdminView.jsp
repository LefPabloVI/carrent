<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 16.12.2019
  Time: 15:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Image</title>
</head>
<body>
<jsp:include page="../template/_menu.jsp"></jsp:include>
<h3>Image Admin</h3>
<h2>${event}</h2>
<form method="POST" action="${pageContext.request.contextPath}/imagesAdmin">
    <input type="hidden" name="id" value="${id}" />
    <input type="hidden" name="event" value="${event}" />
    <table border="0">
        <tr>
            <td>Entity</td>
            <td><input type="text" name="entity" value="${image.entity}"/> </td>
        </tr>
        <tr>
            <td>Element Id</td>
            <td><input type="text" name="elementId" value="${image.elementId}"/> </td>
        </tr>
        <tr>
            <td>Type</td>
            <td><input type="text" name="type" value="${image.type}"/> </td>
        </tr>
        <tr>
            <td>Urn</td>
            <td><input type="text" name="urn" value="${image.urn}"/> </td>
        </tr>
        <tr>
            <td colspan ="2">
                <input type="submit" value= "Submit"/>
                <a href="${pageContext.request.contextPath}/images">Cancel</a>
            </td>
        </tr>
    </table>
</form>
<jsp:include page="../template/_error.jsp"></jsp:include>
</body>
</html>

