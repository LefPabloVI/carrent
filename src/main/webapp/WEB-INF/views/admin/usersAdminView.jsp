<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %><%--
  Created by IntelliJ IDEA.
  User: User
  Date: 04.12.2019
  Time: 12:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Admin</title>
</head>
<%
    Map<String, String> roles = new HashMap<String, String>();
    Map<String, String> statuses = new HashMap<String, String>();

    roles.put("MANAGER", "manger");
    roles.put("EMPLOYEE", "employee");

    statuses.put("BUSY", "busy");
    statuses.put("FREE", "free");
    statuses.put("FIRED", "fired");
    statuses.put("VACATION", "vacation");

    request.setAttribute("roles", roles);
    request.setAttribute("statuses", statuses);
%>
<body>
<jsp:include page="../template/_menu.jsp"></jsp:include>
<h3>Users Admin</h3>
<h2>${event}</h2>
<form method="POST" action="${pageContext.request.contextPath}/usersAdmin">
    <input type="hidden" name="id" value="${id}" />
    <input type="hidden" name="event" value="${event}" />
    <table border="0">
        <tr>
            <td>Name</td>
            <td><input type="text" name="name" value="${user.name}"/> </td>
        </tr>
        <tr>
            <td>Age</td>
            <td><input type="text" name="age" value="${user.age}"/> </td>
        </tr>
        <tr>
            <td>Phone</td>
            <td><input type="text" name="phone" value="${user.phone}"/> </td>
        </tr>
        <tr>
            <td>Passport</td>
            <td><input type="text" name="passport" value="${user.passport}"/> </td>
        </tr>
        <tr>
            <td>Driving Experience</td>
            <td><input type="text" name="drivingExperience" value="${user.drivingExperience}"/> </td>
        </tr>
        <tr>
            <td>Driver License</td>
            <td><input type="text" name="driverLicense" value="${user.driverLicense}"/> </td>
        </tr>
        <tr>
            <td>Email</td>
            <td><input type="text" name="email" value="${user.email}"/> </td>
        </tr>
        <tr>
            <td>employeeCode</td>
            <td><input type="text" name="employeeCode" value="${user.employeeCode}"/> </td>
        </tr>
        <tr>
            <td>Status</td>
            <td>
                <select name="status" id="status">
                    <option value=""></option>
                    <c:forEach var="row" items="${statuses.keySet()}">
                        <c:if test="${row == user.status}">
                            <option selected="selected" value="${row}">${statuses.get(row)}</option>
                        </c:if>
                        <c:if test="${row != user.status}">
                            <option value="${row}">${statuses.get(row)}</option>
                        </c:if>
                    </c:forEach>
                </select>
<%--                <input type="text" name="status" value="${user.status}"/> --%>
            </td>
        </tr>
        <tr>
            <td>Role</td>
            <td>
                <select name="role" id="role">
                    <option value=""></option>
                    <c:forEach var="row" items="${roles.keySet()}">
                        <c:if test="${row == user.role}">
                            <option selected="selected" value="${row}">${roles.get(row)}</option>
                        </c:if>
                        <c:if test="${row != user.role}">
                            <option value="${row}">${roles.get(row)}</option>
                        </c:if>
                    </c:forEach>
<%--                    <%= selectList%>--%>
                </select>
            </td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="text" name="password" value="${user.password}"/> </td>
        </tr>

        <tr>
            <td colspan ="2">
                <input type="submit" value= "Submit"/>
                <a href="${pageContext.request.contextPath}/users">Cancel</a>
            </td>
        </tr>
    </table>
</form>
<jsp:include page="../template/_error.jsp"></jsp:include>
</body>
</html>
