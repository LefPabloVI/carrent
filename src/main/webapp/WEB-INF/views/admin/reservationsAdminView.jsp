<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 06.12.2019
  Time: 9:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Reservations</title>
</head>
<body>
<jsp:include page="../template/_menu.jsp"></jsp:include>
<h3>Reservations Admin</h3>
<h2>${event}</h2>
<form method="POST" action="${pageContext.request.contextPath}/reservationsAdmin">
    <input type="hidden" name="id" value="${id}" />
    <input type="hidden" name="event" value="${event}" />
    <table border="0">
        <tr>
            <td>Car Id</td>
            <td><input type="text" name="car_id" value="${reservation.car}"/> </td>
        </tr>
        <tr>
            <td>Name</td>
            <td><input type="text" name="name" value="${reservation.name}"/> </td>
        </tr>
        <tr>
            <td>Phone</td>
            <td><input type="text" name="phone" value="${reservation.phone}"/> </td>
        </tr>
        <tr>
            <td>Email</td>
            <td><input type="text" name="email" value="${reservation.email}"/> </td>
        </tr>
        <tr>
            <td>Start Date</td>
            <td><input type="date" name="start" value="${reservation.startDate}"/> </td>
        </tr>
        <tr>
            <td>End Date</td>
            <td><input type="date" name="end" value="${reservation.endDate}"/> </td>
        </tr>
        <tr>
            <td colspan ="2">
                <input type="submit" value= "Submit"/>
                <a href="${pageContext.request.contextPath}/reservations">Cancel</a>
            </td>
        </tr>
    </table>
</form>
<jsp:include page="../template/_error.jsp"></jsp:include>
</body>
</html>