<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 09.12.2019
  Time: 9:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Fine</title>
</head>
<body>
<jsp:include page="../template/_menu.jsp"></jsp:include>
<h3>Fine Admin</h3>
<h2>${event}</h2>
<form method="POST" action="${pageContext.request.contextPath}/finesAdmin">
    <input type="hidden" name="id" value="${id}" />
    <input type="hidden" name="event" value="${event}" />
    <table border="0">
        <tr>
            <td>Order Id</td>
            <td><input type="text" name="orderId" value="${fine.order}"/> </td>
        </tr>
        <tr>
            <td>Price</td>
            <td><input type="text" name="price" value="${fine.price}"/> </td>
        </tr>
        <tr>
            <td>Comment</td>
            <td><input type="textarea" name="comment" value="${fine.comment}"/> </td>
        </tr>
        <tr>
            <td colspan ="2">
                <input type="submit" value= "Submit"/>
                <a href="${pageContext.request.contextPath}/fines">Cancel</a>
            </td>
        </tr>
    </table>
</form>
<jsp:include page="../template/_error.jsp"></jsp:include>
</body>
</html>
