<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="entity.hibernate.Price" %><%--
  Created by IntelliJ IDEA.
  User: User
  Date: 06.12.2019
  Time: 12:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Prices</title>
</head>

<%
    Price price = (Price) request.getAttribute("price");
    List<String> values = new ArrayList<String>();
    List<String> prices = new ArrayList<String>();
    prices.add("1-3");
    prices.add("4-7");
    prices.add("8-15");
    prices.add("16-30");

    values.add("F");
    values.add("S");
    values.add("T");
    values.add("FO");

    String selectList = "";
    String selected;
    for ( int i = 0; i < prices.size(); i++) {
        selected = "";
        if (price != null && values.get(i).equalsIgnoreCase(price.getPeriod())) { //must be in this sequence, if other then nullpointer exception
            selected = "selected=\"selected\"";
        }
        selectList += "<option " + selected + " value=\"" + values.get(i) + "\">" + prices.get(i) + "</option>\n";
    }
%>

<body>
<jsp:include page="../template/_menu.jsp"></jsp:include>
<h3>Prices Admin</h3>
<h2>${event}</h2>
<form method="POST" action="${pageContext.request.contextPath}/pricesAdmin">
    <input type="hidden" name="id" value="${id}" />
    <input type="hidden" name="event" value="${event}" />
    <table border="0">
        <tr>
            <td>Car Id</td>
            <td><input type="text" name="car" value="${price.idCar}"/> </td>
        </tr>
        <tr>
            <td>Price</td>
            <td><input type="text" name="price" value="${price.price}"/> </td>
        </tr>
        <tr>
            <td>Period</td>
            <td>
                <select name="period" id="period">
                    <%= selectList%>
                </select>
            </td>
        </tr>

        <tr>
            <td colspan ="2">
                <input type="submit" value= "Submit"/>
                <a href="${pageContext.request.contextPath}/prices">Cancel</a>
            </td>
        </tr>
    </table>
</form>
<jsp:include page="../template/_error.jsp"></jsp:include>
</body>
</html>
